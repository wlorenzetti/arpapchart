# -*- coding: utf-8 -*-
'''
@author:     Walter Lorenzetti, Francesco Belllina
@copyright:  2013-2015 GIS3W. All rights reserved.
@license:    GPL2
@contact:    lorenzetti@gis3w.it
'''

from globals import *
from arpapchart.utils import *
import psycopg2
import psycopg2.errorcodes
import psycopg2.extras
import psycopg2.extensions
psycopg2.extensions.register_type(psycopg2.extensions.UNICODE)
psycopg2.extensions.register_type(psycopg2.extensions.UNICODEARRAY)
import pprint
import sys
from .globals import *

class db:
    dbHost = DBHOST
    dbName = DBNAME
    dbUser = DBUSER
    dbPswd = DBPSWD
    dbSchema = DBSCHEMA
    conn = None
    cur = None
    def __init__(self):
        self.connect()
        
    def connect(self):
        try:
            self.conn = psycopg2.connect("dbname='"+self.dbName+"' user='"+self.dbUser+"' host='"+self.dbHost+"' password='"+self.dbPswd+"'")
            self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
            return self
        except psycopg2.Error  as e:
            showMessage(None, 'critical','Errore - cod. '+str(e.pgcode), str(e.message))
            print e.message
            sys.exit(0)
            
    def disconnect(self):
        try:
            self.cur.close()
            self.conn.close()
        except psycopg2.Error as e:
            showMessage(None, 'critical','Errore - cod. '+str(e.pgcode), str(e.message))
            print e.message
            sys.exit(0)
        
    def query(self, sqlString):
        try:
            self.cur.execute(sqlString)
        except Exception, e:
            showMessage(None, 'critical','Errore - cod. '+str(e.pgcode), str(e.message))
            print e.message
            self.disconnect()
            sys.exit(0)
        else:
            if self.cur.rowcount == -1:
                return None
            else:
                return self.cur.fetchall()    
    
    def isMultiGroupChart(self, chartId):
        sql = "SELECT multiple_selection FROM " + DBSCHEMAADMIN + ".grafici WHERE id_graf = '"+str(chartId)+"';"
        obj = self.query(sql)
        bMultiple = obj[0]['multiple_selection']
        return bMultiple        
        
    def getTables(self):
        sql = "SELECT table_name FROM information_schema.tables WHERE table_schema = '" + self.dbSchema + "';"
        return self.query(sql)
    
    def getRaggrupIdsByChart(self, chartId):
        sql = "SELECT raggrup FROM " + DBSCHEMAADMIN + ".grafici WHERE id_graf = '"+str(chartId)+"' AND raggrup <> '';"
        return self.query(sql)
    
    def getChartTypes(self):
        sql = "SELECT * FROM " + DBSCHEMAADMIN + ".tipo_grafici ORDER BY tipo_grafico;"
        return self.query(sql)
    
    def getChartGroups(self):
        sql = "SELECT * FROM " + DBSCHEMAADMIN + ".gruppo_grafico;"
        return self.query(sql)
    
    def getChartById(self, grafId):
        sql = "SELECT * FROM " + DBSCHEMAADMIN + ".grafici WHERE id_graf = "+str(grafId)+";"
        return self.query(sql)
    
    def getChartByCode(self, code):
        sql = "SELECT * FROM " + DBSCHEMAADMIN + ".grafici WHERE num_grafico = '"+str(code)+"';"
        return self.query(sql)
    
    def getChartsByGroup(self, groupId):
        sql = "SELECT * FROM " + DBSCHEMAADMIN + ".grafici WHERE id_group = "+str(groupId)+" ORDER BY id_graf;"
        return self.query(sql)
    
    def getFieldsByTable(self, table):
        sql = "SELECT column_name FROM information_schema.columns WHERE table_schema = '" + self.dbSchema + "' AND table_name = '"+str(table)+"' AND (data_type in ('smallint', 'integer', 'bigint', 'decimal', 'numeric', 'real', 'double precision', 'serial', 'bigserial', 'date'));"
        return self.query(sql)
    
    def getDateFieldsByTable(self, table):
        sql = "SELECT column_name FROM information_schema.columns WHERE table_schema = '" + self.dbSchema + "' AND table_name = '"+str(table)+"' AND data_type = 'date';"
        return self.query(sql)
    
    def getAllRaggrupValuesUsingCustomQuery(self, chartId): # ritorno None se non e' settata alcuna query customizzata, altrimenti una lista
        sql = "SELECT raggrup_query FROM " + DBSCHEMAADMIN + ".grafici WHERE id_graf = '"+str(chartId)+"';"
        data = self.query(sql)
        if (data is not None and len(data) > 0):
            customQuery = convert(data[0]['raggrup_query'], "string")
            if (customQuery is not None):
                sql = customQuery.replace("{{id_graf}}", str(chartId));
                obj = self.query(sql)
                aux = []
                if (obj is not None and len(obj) > 0):
                    for j in obj:
                        if (j[0] is not None):
                            aux.append(j[0])
                return aux
        else:
            return None
    
    def getAllRaggrupValues(self, grafId):
        sql = "SELECT * FROM " + DBSCHEMAADMIN + ".grafici WHERE id_graf = "+str(grafId)+";"
        obj = self.query(sql)
        if (len(obj) > 0):
            where = ''
            if (obj[0]['sel_where'] is not None):
                where = " WHERE "+str(obj[0]['sel_where'])
            customQuery = "SELECT DISTINCT "+str(obj[0]['raggrup'])+" FROM "+str(obj[0]['sel_from'])+where+" ORDER BY "+str(obj[0]['raggrup'])
            obj2 = self.query(customQuery)
        else:
            obj2 = []
        aux = []
        if (obj2 is not None and len(obj2) > 0):
            for j in obj2:
                aux.append(convert(j[0],'string'))
        return aux

    def getAllReggrupValuesByNumberChart(self, NumberChart):
        sql = "SELECT * FROM " + DBSCHEMAADMIN + ".grafici WHERE num_grafico = '"+str(NumberChart)+"';"
        obj = self.query(sql)
        return self.getAllRaggrupValues(obj[0]['id_graf'])
    
    def getAllSubgroupValues(self, grafId, raggrupId):
        sql = "SELECT * FROM " + DBSCHEMAADMIN + ".grafici WHERE id_graf = "+str(grafId)+";"
        obj = self.query(sql)
        
        if (len(obj) > 0):
            if (obj[0]['subgroup_query'] is not None):
                customQuery = convert(obj[0]['subgroup_query'], "string")
                if (customQuery is not None):
                    sql = customQuery.replace("{{id_graf}}", str(grafId));
                    sql = customQuery.replace("{{raggrup}}", str(raggrupId));
                    obj2 = self.query(sql)
            else:
                if (obj[0]['subgroup'] is not None):
                    where = "WHERE "+str(obj[0]['raggrup'])+" = "+str(raggrupId)
                    if (obj[0]['sel_where'] is not None):
                        where += " AND "+str(obj[0]['sel_where'])
                    customQuery = "SELECT DISTINCT "+str(obj[0]['subgroup'])+" FROM "+str(obj[0]['sel_from'])+" "+where+" ORDER BY "+str(obj[0]['subgroup'])
                    obj2 = self.query(customQuery)
                else:
                    return None
        else:
            return None
            
        aux = []
        if (obj2 is not None and len(obj2) > 0):
            for j in obj2:
                if (j[0] is not None):
                    aux.append(convert(j[0],'string'))
            return aux
        return None
    
    def getData2d(self, table, xField, yField):
        sql = "SELECT "+str(xField)+","+str(yField)+" FROM "+ self.dbSchema + "." + str(table) + " ORDER BY " + str(xField)
        return [self.query(sql)]
    
    def getDataByParams(self, grafId, raggrupId):
        sql = "SELECT * FROM " + DBSCHEMAADMIN + ".grafici WHERE id_graf = "+str(grafId)+";"
        obj = self.query(sql)
        orderBy = ""
        
        if (len(obj) > 0):
            if (obj[0]['raggrup'] == None or obj[0]['raggrup'] == ''):
                raggrupField = None
            else:
                raggrupField = str(obj[0]['raggrup'])
            customQuery = "SELECT "+str(obj[0]['sel_select'])+" FROM "+str(obj[0]['sel_from'])
            if (obj[0]['sel_where'] != None or (raggrupField != None and raggrupId != None)):
                customQuery += " WHERE "
                if obj[0]['sel_where'] != None:
                    customQuery += str(obj[0]['sel_where'])
                    if (raggrupField != None and raggrupId != None):
                        customQuery += " AND "
                if (raggrupField != None and raggrupId != None):
                    customQuery += str(raggrupField)+" IN ("+str(raggrupId)+") "
                    orderBy += str(raggrupField)
                    if (obj[0]['subgroup'] != None and obj[0]['subgroup'] != ''):
                        orderBy += ","+obj[0]['subgroup']
            #customQuery += " ORDER BY "+obj[0]['x']+","+obj[0]['y']
            if obj[0]['sel_order_by'] != None:
                if (orderBy != ""):
                    orderBy += ','
                orderBy += obj[0]['sel_order_by']
            customQuery += " ORDER BY "+orderBy
                
            objResult = self.query(customQuery)
            
            if (raggrupField == None):
                return [objResult]
                
            helpObj = {}            
            for value in objResult:
                if not (value[raggrupField] in helpObj):
                    helpObj[value[raggrupField]] = []
                helpObj[value[raggrupField]].append(value)
                
            objToReturn = []
            for key in helpObj.keys():
                objToReturn.append(helpObj[key])
            
            return objToReturn
        else:
            return None
        
    def getDefaultParameters(self, chartId):
        sql = "SELECT default_parameters FROM " + DBSCHEMAADMIN + ".grafici WHERE id_graf = '"+str(chartId)+"';"
        obj = self.query(sql)
        if (obj and obj[0] and obj[0][0]):
            return obj[0][0]

    def isGroupChartUnderAuthorization(self, groupChartId):
        sql = "SELECT * FROM " + DBSCHEMAADMIN + ".gruppo_grafico WHERE id_group = "+str(groupChartId)+" and (users is not null OR roles is not null);"
        obj = self.query(sql)
        return bool(len(obj))

    def getUsersForGroupChart(self,groupChartId):
        sql = "SELECT users FROM " + DBSCHEMAADMIN + ".gruppo_grafico WHERE id_group = "+str(groupChartId)+";"
        obj = self.query(sql)
        if obj[0][0]:
            return obj[0][0].split(',')
        else:
            return []

    def getRolesForGroupChart(self,groupChartId):
        sql = "SELECT roles FROM " + DBSCHEMAADMIN + ".gruppo_grafico WHERE id_group = "+str(groupChartId)+";"
        obj = self.query(sql)
        if obj[0][0]:
            return obj[0][0].split(',')
        else:
            return []

    def getChartTypeByNumberChart(self,numberChart):
        sql = "SELECT id_tipo from " + DBSCHEMAADMIN + ".grafici WHERE num_grafico = '"+str(numberChart)+"';"
        obj = self.query(sql)
        if obj[0][0]:
            return obj[0][0]
        else:
            return None

    def getGroupChartByNumberChart(self,numberChart):
        sql = "SELECT id_group from " + DBSCHEMAADMIN + ".grafici WHERE num_grafico = '"+str(numberChart)+"';"
        obj = self.query(sql)
        if obj[0][0]:
            return obj[0][0]
        else:
            return None
