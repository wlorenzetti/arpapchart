# -*- coding: utf-8 -*-
'''
@author:     Walter Lorenzetti, Francesco Belllina
@copyright:  2013-2015 GIS3W. All rights reserved.
@license:    GPL2
@contact:    lorenzetti@gis3w.it
'''

from arpapchart.globals import *
from arpapchart.dbLib import db
import matplotlib.pyplot as plt
from arpapchart.chartLib import chart as chartLib
import os, sys
import pprint

class chart():
    
    myPGDB = None
    myChart = None
    plt = None
    args = None
    
    def __init__(self,args):
        self.args = args
        self.myPGDB = db()
        self.plt = plt
        self.plt.fig, self.plt.ax = self.plt.subplots()
        self.myChart = chartLib(self.plt)
        
        # set the section
        self.myChart.currentEnvironment = ENVIROIMENTS['CLI']
        
        # set chart options
        self.myChart.pointsViewer = self.args.show_points
        self.myChart.logxScale = self.args.logaritmic_scale_x
        self.myChart.logyScale = self.args.logaritmic_scale_y
        
        idRaggr = self.args.raggrup_id != None and ','.join(self.args.raggrup_id) or None
        
        if self.args.code_chart != None:
            # get the id for chart
           res = self.myPGDB.getChartByCode(self.args.code_chart)
           if not res:
               pass
           idGraf = res[0][0]
           self.myChart.buildByChart(idGraf, idRaggr, self.args.minimun_x, self.args.maximun_x, self.args.minimun_y, self.args.maximun_y)
           if(self.args.image_path != None):
               self.myChart.saveImage(self.args.image_path, dpi=CLI_DEFAULT_VALUES['CHART_DPI'])
           else:
               self.plt.show()
                
        else:
            pass
        