# -*- coding: utf-8 -*-
'''
@author:     Walter Lorenzetti, Francesco Belllina
@copyright:  2013-2015 GIS3W. All rights reserved.
@license:    GPL2
@contact:    lorenzetti@gis3w.it
'''

WEBDEBUG = True

VERSION = '1.2.10'

ENVIROIMENTS = {
                    'DESKTOP' : 0,
                    'WEB' : 1,
                    'CLI' : 2,
                }

WEB_SESSION_SECRETKEY = 'arpapchart89'

WEB_URLROOTPATH = ''

WEB_DEFAULT_VALUES = {
                      'CHART_WIDTH_PX' : 800,
                      'CHART_HEIGHT_PX': 600,
                      'CHART_DPI': 96,
                      'X_N_TICKS': 5,
                      'Y_N_TICKS': 5,
                      'X_ROTATION_TICKS': 0,
                      'LINE_WIDTH': 3,
                      'LOG_FILE': 'web.log',
                      }
CLI_DEFAULT_VALUES = {
                      'CHART_WIDTH_PX' : 800,
                      'CHART_HEIGHT_PX': 600,
                      'CHART_DPI': 96
                      }

from .settings.db_settings import *
    
#1;"Spostamenti sugli assi x e y"
#2;"Spostamenti sulle ordinate e tempo sulle ascisse"
#3;"Polare"
#4;"Spostamenti e curve di riferimento"
#5;"Stratigrafia"
#6;"Confronto tra differenti strumenti"
#7;"Classificazione USCS"
