# -*- coding: utf-8 -*-
'''
@author:     Walter Lorenzetti, Francesco Belllina
@copyright:  2013-2015 GIS3W. All rights reserved.
@license:    GPL2
@contact:    lorenzetti@gis3w.it
'''

from arpapchart.globals import *
from arpapchart.utils import *
from arpapchart.dbLib import db
from matplotlib import *
from PIL import Image
import math
import datetime
from time import strftime
from arpapchart.desktop.mplwidget import *
import os
import pprint
import numpy as np
from collections import OrderedDict

class chart:
    myUI = None
    myCanvas = None
    myLines = None
    myBars = []
    myScalarmap = None
    myPointSelector = None
    myAnnotations = []
    mySelectedArtist = None
    bDefaultParams = False
    pickEventId = None
    chartType = None
    raggrupField = None
    selectedGroups = []
    selectedSubgroups = []
    subgroupField = None
    subgroupList = []
    groupList = [] #serve solo per il nome dell'unico gruppo presente per il grafico della stratigrafia
    tableName = None
    chartId = None
    chartLabel = None
    myLegend = None
    xLabelsList = []
    xField = None
    xLabel = None
    xMin = None
    xMax = None
    xTicks = None
    xGrid = None
    yGrid = None
    yField = None
    yLabel = None
    yMin = None
    yMax = None
    yTicks = None
    pointsViewer = None
    bGrid = None
    logxScale = None
    logyScale = None
    legendLoc = 7
    lineWidth = 3
    xRotationTicks = 0
    logoImagePath = os.path.dirname(os.path.abspath(__file__))+'/../logo.png'
    saveImgDir = '/runserver/static/img/cache/'
    currentEnvironment = ENVIROIMENTS['DESKTOP']
     
    def __init__(self, currentPlot, ui = None):
        self.myCanvas = currentPlot
        self.myUI = ui
        self.myUI = ui

    def insertCompanyLogo(self):
        im = Image.open(self.logoImagePath)
        im = im.resize((80,39))
        #print self.currentEnvironment

        if(self.currentEnvironment == ENVIROIMENTS['WEB']):
            size = self.myCanvas.fig.get_size_inches()
            xmax = int(size[0]*self.myCanvas.fig.get_dpi())
            self.myCanvas.fig.figimage(im,xmax + xmax*0.05,10)
        else:
            self.myCanvas.fig.figimage(im)
        
    
    def redraw(self):
        try:
            self.myCanvas.draw()
        except:
            print "Errore in redraw"
            
    def formatLegend(self, listOfLines = None):
        if (listOfLines is None):
            self.myLegend = self.myCanvas.ax.legend(loc=self.legendLoc,ncol=2,shadow=False,fancybox=False,title="Legenda",fontsize=10)
        else:
            labels = []
            for line in listOfLines:
                labels.append(line.get_label())
            self.myLegend = self.myCanvas.ax.legend(listOfLines, labels, loc=self.legendLoc,ncol=2,shadow=False,fancybox=False,title="Legenda",fontsize=10)
        if (self.myLegend is not None):
            self.myLegend.get_frame().set_alpha(0.5)
            self.myLegend.draggable()
    
    def updateLegend(self, listOfLines = []):
        if (self.myLegend is None):
            return
        if (len(listOfLines) > 0):
            if (not self.myCanvas.ax.legend().get_visible()):
                self.myCanvas.ax.legend().set_visible(True)
            self.formatLegend(listOfLines)
        else:
            if(self.currentEnvironment == ENVIROIMENTS['DESKTOP']):
                if (self.myCanvas.ax.legend().get_visible()):
                    self.myCanvas.ax.legend().set_visible(False)
    
    def chartSetup(self, nGroups=1):
        if (self.myCanvas.ax is not None and self.myCanvas.fig is not None):
            self.myCanvas.fig.delaxes(self.myCanvas.ax)
        if (self.chartType == 3): #polare
            self.myCanvas.ax = self.myCanvas.fig.add_subplot(111, projection='polar')
            self.myCanvas.ax.set_theta_zero_location("N")
            self.myCanvas.ax.set_theta_direction("clockwise")
        else:
            self.myCanvas.ax = self.myCanvas.fig.add_subplot(111)
        if (self.xLabel is None):
            self.xLabel = ''
        if (self.yLabel is None):
            self.yLabel = ''
            
        self.myCanvas.ax.set_xlabel(self.xLabel.decode("UTF-8"))
        self.myCanvas.ax.set_ylabel(self.yLabel.decode("UTF-8"))
        self.myCanvas.ax.set_title(self.chartLabel, fontdict={'fontsize': 20,'verticalalignment':'bottom'})
        cNorm  = colors.Normalize(vmin=0, vmax=nGroups-1)
        self.myScalarmap = cm.ScalarMappable(norm=cNorm, cmap=cm.get_cmap('Set1'))    
    
    def setLocator(self, axis, spaceGrid, bDraw):
        if (self.myCanvas.ax is None):
            return
        try:
            val = convert(spaceGrid, "int")
            if (val is None):
                return
            if (axis == 'x'):
                self.myCanvas.ax.xaxis.reset_ticks()
                self.myCanvas.ax.xaxis.set_major_locator(ticker.LinearLocator(val))
                if (self.chartType == 3):
                    rxt = self.myCanvas.ax.get_xticks()
                    dxt = []
                    for t in rxt:
                        d = math.degrees(t)
                        if (d != 360):
                            dxt.append(d)
                    self.myCanvas.ax.set_thetagrids(dxt)
                
                if (self.chartType == 2 and self.xRotationTicks < 45): #date
                    self.myCanvas.fig.autofmt_xdate(rotation=45)
                else:
                    self.myCanvas.fig.autofmt_xdate(rotation=self.xRotationTicks)
            if (axis == 'y'):
                self.myCanvas.ax.yaxis.reset_ticks()
                self.myCanvas.ax.yaxis.set_major_locator(ticker.LinearLocator(val))
            self.setTicksFontsize()
            if (bDraw):
                self.myCanvas.draw()
                '''
                xt = self.myCanvas.ax.get_xticks()
                yt = self.myCanvas.ax.get_yticks()
                self.myCanvas.ax.set_xticklabels(xt)
                self.myCanvas.ax.set_yticklabels(yt)
                '''
                
        except:
            print "exception in setLocator"
           
    def onPick(self, params):
        if (self.myPointSelector is not None):
            self.PointSelectorRemove()
        if (params.artist is None or not isinstance(params.artist, lines.Line2D)):
            return
        self.mySelectedArtist = params.artist
        if (self.chartType != 2 and self.chartType != 3):
            myX = None
            myY = None
            ind = params.ind
            nextInd = params.ind+1
            xData = params.artist.get_xdata(True)
            yData = params.artist.get_ydata(True)
            xdata0 = np.float64(np.take(xData, ind)[0])
            ydata0 = np.float64(np.take(yData, ind)[0])
            try:
                xdata1 = np.float64(np.take(params.artist.get_xdata(True), nextInd)[0])
                ydata1 = np.float64(np.take(params.artist.get_ydata(True), nextInd)[0])
            except:
                myX = xdata0
                myY = ydata0
            else:
                if (self.logxScale or self.logyScale):
                    '''xx1 = params.mouseevent.xdata - xdata0
                    xx2 = xdata1 - params.mouseevent.xdata
                    arr = [xx1,xx2]
                    xminimo = np.min(arr)
                    if (xminimo == xx1):
                        myX = xdata0
                        myY = ydata0
                    else:'''
                    myX = params.mouseevent.xdata
                    myY = params.mouseevent.ydata
                else:
                    if (xdata1-xdata0 != 0):
                        m = (ydata1-ydata0)/(xdata1-xdata0)
                        myX_x = params.mouseevent.xdata
                        myY_x = m*(myX_x-xdata0)+ydata0
                        yminima = np.min([myY_x, params.mouseevent.ydata])
                        ymassima = np.max([myY_x, params.mouseevent.ydata])
                        erroreY = ymassima-yminima
                        myY_y = params.mouseevent.ydata
                        if (m == 0):
                            myY = myY_x
                            myX = myX_x
                        else:
                            myX_y = xdata0+(myY_y-ydata0)/m
                            xminima = np.min([myX_y, params.mouseevent.xdata])
                            xmassima = np.max([myX_y, params.mouseevent.xdata])
                            erroreX = xmassima-xminima
                            erroreMinimo = np.min([erroreX,erroreY])
                            if (erroreMinimo == erroreX):
                                myY = myY_y
                                myX = myX_y
                            else:
                                myY = myY_x
                                myX = myX_x
                            
                        '''
                        yminima = np.min([ydata0, ydata1])
                        ymassima = np.max([ydata0, ydata1])
                        if (myY > ymassima or myY < yminima):
                            myY = params.mouseevent.ydata
                            myX = xdata0+(myY-ydata0)/m
                            xminima = np.min([xdata0, xdata1])
                            xmassima = np.max([xdata0, xdata1])
                            if (myY > xmassima or myY < xminima):
                                myX = xdata0
                                myY = params.mouseevent.ydata
                        '''
                    else:
                        myX = xdata0
                        myY = params.mouseevent.ydata
            finally:
                self.PointSelectorDraw(myX, myY, params.artist)
                self.myUI.selectedX.setText(convert(myX, "string"))
                self.myUI.selectedY.setText(convert(myY, "string"))
        else:
            myX = params.mouseevent.xdata
            myY = params.mouseevent.ydata
            self.PointSelectorDraw(myX, myY, params.artist)
            if (self.chartType == 2):
                self.myUI.selectedX.setText(dates.num2date(convert(myX,"float")).strftime('%d/%m/%Y'))
            else:
                self.myUI.selectedX.setText(convert(math.degrees(myX), "string"))
            self.myUI.selectedY.setText(convert(myY, "string"))
        if (self.raggrupField is not None):
            if (self.subgroupField is not None):
                self.myUI.selectedSubgroup.setText(params.artist.get_label())
                if (self.selectedGroups is not None and len(self.selectedGroups) == 1):
                    self.myUI.selectedGroup.setText(convert(self.selectedGroups[0],'string'))
                else:
                    self.myUI.selectedGroup.clear()
            else:
                self.myUI.selectedGroup.setText(params.artist.get_label())
                self.myUI.selectedSubgroup.clear()
        else:
            self.myUI.selectedGroup.clear()
            self.myUI.selectedSubgroup.clear()
        self.myUI.showAnnotations.setEnabled(True)
        if (self.myUI.showAnnotations.isChecked()):
            self.removeAllAnnotations()
            self.toggleAnnotations(True)
        
    def setLimits(self, bDraw):
        if (self.myCanvas.ax is None):
            return
        if (self.chartType == 5): #stratigrafia
            self.xMin = -0.2
            self.xMax = 5
            self.yMin = None
            self.yMax = None
            xMax = self.xMax
            xMin = self.xMin
        else:
            myxType = "float"
            if(self.currentEnvironment == ENVIROIMENTS['DESKTOP']):
                if (self.chartType == 2):
                    myxType = "datenum"
                if (self.chartType == 3):
                    if (self.myUI.xMin.text() != ''):
                        xMin=convert(math.radians(convert(self.myUI.xMin.text(), myxType)),myxType)
                    else:
                        xMin = None
                    if (self.myUI.xMax.text() != ''):
                        xMax=convert(math.radians(convert(self.myUI.xMax.text(), myxType)),myxType)
                    else:
                        xMax = None
                else:
                    xMin=convert(self.myUI.xMin.text(), myxType)
                    xMax=convert(self.myUI.xMax.text(), myxType)
                yMin=convert(self.myUI.yMin.text(), "float")
                yMax=convert(self.myUI.yMax.text(), "float")
            else:
                xMin=self.xMin
                xMax=self.xMax
                yMin=self.yMin
                yMax=self.yMax
            self.myCanvas.ax.relim()
            self.myCanvas.ax.autoscale_view()

            if self.chartType == 8:
                for l in self.myBars:
                    barDataY = [b.get_height() for b in l]
                    tmpxmin = 0
                    tmpxmax = len(l) - 1
                    tmpymin = min(barDataY)
                    tmpymax = max(barDataY)
                    # get index from np array x
                    npaxx = np.array(self.groupList,dtype='datetime64')
                    if xMin is not None:
                        self.xMin = np.argwhere(npaxx==xMin)
                        if self.xMin:
                            self.xMin = self.xMin[0][0]
                    if xMax is not None:
                        self.xMax = np.argwhere(npaxx==self.xMax)[0]

                    if (xMin is None and (self.xMin is None or tmpxmin < self.xMin)):
                        self.xMin = tmpxmin
                    if (xMax is None and (self.xMax is None or tmpxmax > self.xMax)):
                        self.xMax = tmpxmax
                    if (yMin is None and (self.yMin is None or tmpymin < self.yMin)):
                        self.yMin = tmpymin
                    if (yMax is None and (self.yMax is None or tmpymax > self.yMax)):
                        self.yMax = tmpymax


            for l in self.myLines:
                lineData = l.get_data(False)
                if (len(lineData[0]) == 0 or len(lineData[1]) == 0):
                    return
                tmpxmin = lineData[0].min()
                tmpxmax = lineData[0].max()
                tmpymin = lineData[1].min()
                tmpymax = lineData[1].max()
                if (xMin is None and (self.xMin is None or tmpxmin < self.xMin)):
                    self.xMin = tmpxmin
                if (xMax is None and (self.xMax is None or tmpxmax > self.xMax)):
                    self.xMax = tmpxmax
                if (yMin is None and (self.yMin is None or tmpymin < self.yMin)):
                    self.yMin = tmpymin
                if (yMax is None and (self.yMax is None or tmpymax > self.yMax)):
                    self.yMax = tmpymax

            if (xMin is not None):
                self.xMin = xMin#convert(xMin, myType)
            if (xMax is not None):
                self.xMax = xMax#convert(xMax, myType)
            if (yMin is not None):
                self.yMin = yMin#convert(yMin, myType)
            if (yMax is not None):
                self.yMax = yMax#convert(yMax, myType)

        if (self.xMax is not None and self.xMin is not None):
            diffX = abs(self.xMax-self.xMin)*0.05
        else:
            diffX = None
        if (self.yMax is not None and self.yMin is not None):
            diffY = abs(self.yMax-self.yMin)*0.05
        else:
            diffY = None
        if (self.xMin < self.xMax):
            if (self.xMin is None and diffX is None):
                ll = None
            else:
                ll = self.xMin
                '''if (self.xMin is not None and diffX is not None):
                    ll = self.xMin-diffX'''
            if (self.xMax is None and diffX is None): #and perche' c'e' la stratigrafia
                rl = None
            else:
                rl = self.xMax # perche' nel caso della stratigrafia in cui xmax e' 5
                if (self.xMax is not None and diffX is not None and xMax is None):
                    rl = self.xMax+diffX
            self.myCanvas.ax.set_xlim(left=ll, right=rl)
        if (self.yMin < self.yMax):
            if (self.yMin is None or diffY is None):
                bl = None
            else:
                bl = self.yMin
                '''if (self.yMin is not None and diffY is not None):
                    bl = self.yMin-diffY'''
            if (self.yMax is None or diffY is None):
                tl = None
            else:
                tl = self.yMax
                if (self.yMax is not None and diffY is not None and yMax is None):
                    tl = self.yMax+diffY
            #print 'bl: '+str(bl)+' / tl: '+str(tl)
            self.myCanvas.ax.set_ylim(bottom=bl, top=tl)
        
        if (isinstance(self.myCanvas, MplCanvas) and bDraw):
            self.myCanvas.draw()
    
    def toggleLegend(self, bView):
        if (self.myLegend is None):
            return
        self.myLegend.set_visible(bView)
            
    def toggleOptionsFields(self, bEnable):
        if(self.currentEnvironment == ENVIROIMENTS['DESKTOP']):
            self.myUI.pointsViewer.setEnabled(bEnable)
            self.myUI.xMin.setEnabled(bEnable)
            self.myUI.xMax.setEnabled(bEnable)
            self.myUI.yMin.setEnabled(bEnable)
            self.myUI.yMax.setEnabled(bEnable)
            self.myUI.logxScale.setEnabled(bEnable)
            self.myUI.logyScale.setEnabled(bEnable)
            self.myUI.xgrid.setEnabled(bEnable)
            self.myUI.ygrid.setEnabled(bEnable)
    
    def setTicksFontsize(self, size = None):
        mySize = convert(size, "int")
        if (mySize is None):
            mySize = 10
        if (self.myCanvas is None or self.myCanvas.ax is None):
            return
        tl = self.myCanvas.ax.get_xticklabels()
        for label in tl: 
            label.set_fontsize(mySize)
        tl = self.myCanvas.ax.get_yticklabels()
        for label in tl: 
            label.set_fontsize(mySize)
            
    
    def oncePlotted(self):
        self.toggleGrid(self.bGrid)
        self.togglePoints(self.pointsViewer)        
        self.toggleScale('x', self.logxScale)
        self.toggleScale('y', self.logyScale)
        if(self.currentEnvironment == ENVIROIMENTS['DESKTOP']):
            self.setLocator('x', self.myUI.xgrid.text(), False)
            self.setLocator('y', self.myUI.ygrid.text(), False)
        if(self.currentEnvironment == ENVIROIMENTS['WEB']):
            self.setLocator('x', self.xGrid, False)
            self.setLocator('y', self.yGrid, False)
        if (self.chartId == 24): # USCS
            self.myCanvas.ax.axhline(30, color='k')
            self.myCanvas.ax.axhline(50, color='k')
            self.myCanvas.ax.axvline(7.25, color='k')
            self.myCanvas.ax.axvline(4, color='k')
            # y=0,725x-14,5
            self.myCanvas.ax.plot([-140, -60, 60, 140], [-116, -58, 29, 87], color='k')
        self.setLimits(False)        
        
        '''if(self.myUI != None):
            self.toggleOptionsFields(False)'''
        
        '''invertY = [31]
        if (self.chartType == 5 or self.chartId in invertY):
            self.myCanvas.ax.invert_yaxis()
        else:
            if (self.myCanvas.ax.yaxis_inverted()):
                self.myCanvas.ax.invert_yaxis()'''
        
        if (self.raggrupField != None):
            if (self.chartType == 5):
                self.myLegend = None
                self.myCanvas.ax.set_yticks(self.yTicks)
                self.myCanvas.ax.set_yticklabels(self.myCanvas.ax.get_yticks(), fontsize=8)
                self.myCanvas.ax.set_xticks(self.xTicks)
                self.myCanvas.ax.set_xticklabels(self.groupList)
            elif (self.chartType == 8):
                '''
                if self.xMax != None and self.xMin != None:
                    xlabels = self.groupList[self.xMin,self.xMax]
                elif self.xMax != None:
                    xlabels = self.groupList[0,self.xMax]
                elif self.xMin != None:
                    xlabels = self.groupList[self.xMin,]
                else:
                '''
                xlabels = self.groupList
                self.myCanvas.ax.set_xticks(np.arange(len(xlabels)))
                self.myCanvas.ax.set_xticklabels(xlabels)
            else:
                '''if (self.chartType != 2 and self.myUI != None):
                    self.toggleOptionsFields(True)'''
                self.formatLegend()
        if (self.chartType == 2):
            #self.myCanvas.ax.set_xticks(self.xTicks)
            #self.myCanvas.ax.set_xticklabels(self.xLabelsList)
            #self.myCanvas.ax.set_xlim(min(self.xTicks), max(self.xTicks))
            
            #self.myCanvas.ax.xaxis.set_major_locator(ticker.MultipleLocator(10))
            #self.myCanvas.ax.xaxis.set_major_formatter(dates.DateFormatter('%m %Y'))
            #self.myCanvas.ax.xaxis.set_major_locator(ticker.LinearLocator())
            self.myCanvas.ax.xaxis.set_major_formatter(dates.DateFormatter('%d/%m/%Y'))
            if (self.xRotationTicks < 45):
                self.myCanvas.fig.autofmt_xdate(rotation=45)
            else:
                self.myCanvas.fig.autofmt_xdate(rotation=self.xRotationTicks)

        if(self.currentEnvironment == ENVIROIMENTS['DESKTOP']):
            self.myCanvas.mpl_disconnect(self.pickEventId)
            if (self.chartId == 14 or self.chartId == 24): # piezometro, USCS
                self.myUI.pointsViewer.setChecked(True)
                self.myUI.pointsViewer.setEnabled(False)        
        
        self.setTicksFontsize()
        
        self.pickEventId = None
        if (isinstance(self.myCanvas, MplCanvas) and self.chartType != 5 and self.currentEnvironment == ENVIROIMENTS['DESKTOP']):
            self.pickEventId = self.myCanvas.mpl_connect('pick_event', self.onPick)
        if(self.currentEnvironment == ENVIROIMENTS['DESKTOP']):
            self.toggleInversionAxis('x', self.myUI.invert_xaxis.isChecked(), False)
            self.toggleInversionAxis('y', self.myUI.invert_yaxis.isChecked(), False)
        if (isinstance(self.myCanvas, MplCanvas)):
            self.myCanvas.draw()
    
    def PointSelectorDraw(self, x, y, artist):        
        self.myPointSelector, = self.myCanvas.ax.plot(x, y, 's', color = artist.get_color(), markersize=11, markeredgecolor="black")
        if (isinstance(self.myCanvas, MplCanvas)):
            self.myCanvas.draw()
    
    def PointSelectorRemove(self):
        if (self.myPointSelector is None):
            return
        self.myPointSelector.remove()
        self.myPointSelector = None
        self.removeAllAnnotations()
        if (isinstance(self.myCanvas, MplCanvas)):
            self.myCanvas.draw()
        
    def removeAllAnnotations(self):
        for currAnn in self.myAnnotations:
            currAnn.remove()
        self.myAnnotations = []
        
    def toggleAnnotations(self, bShow):
        if (self.mySelectedArtist is None):
            return
        if (bShow):
            for currMarker in self.mySelectedArtist.get_xydata():
                etichetta = ""
                if (self.chartType == 2):
                    etichetta = str(dates.num2date(convert(currMarker[0],"float")).strftime('%d/%m/%Y')+' ; '+str(currMarker[1]))
                else:
                    if (self.chartType==3):
                        etichetta = str(math.degrees(currMarker[0]))+' ; '+str(currMarker[1])
                    else:
                        etichetta = str(currMarker[0])+' ; '+str(currMarker[1])
                self.myAnnotations.append(self.myCanvas.ax.annotate(etichetta, xy = (currMarker[0], currMarker[1]), xytext = (20, 25), textcoords = 'offset points', ha = 'right', va = 'top', bbox = dict(boxstyle = 'round,pad=0.5', fc = 'yellow', alpha = 0.5), arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0'), fontproperties=matplotlib.font_manager.FontProperties(size=8)))
        else:
            self.removeAllAnnotations()
        if (isinstance(self.myCanvas, MplCanvas)):
            self.myCanvas.draw()    
    
    def toggleAllGroups(self, bView):
        if(self.currentEnvironment == ENVIROIMENTS['DESKTOP']):
            if (bView):
                self.myUI.raggrupId.selectAll()
            else:
                self.myUI.raggrupId.clearSelection()
    
    def toggleAllSubgroups(self, bView):
        if(self.currentEnvironment == ENVIROIMENTS['DESKTOP']):
            if (bView):
                self.myUI.subgroupId.selectAll()
            else:
                self.myUI.subgroupId.clearSelection()
        
    def toggleGrid(self, bGrid):
        self.bGrid = bGrid
        if (self.myCanvas.ax is not None):
            self.myCanvas.ax.grid(b=self.bGrid)
        if (isinstance(self.myCanvas, MplCanvas)):
            if (self.myCanvas.ax is not None):
                self.myCanvas.draw()
    
    def toggleInversionAxis(self, axis, bChecked, bDraw):
        if (self.myCanvas.ax == None):
            return
        if (axis == 'x'):
            if (bChecked):
                if (not self.myCanvas.ax.xaxis_inverted()):
                    self.myCanvas.ax.invert_xaxis()
            else:
                if (self.myCanvas.ax.xaxis_inverted()):
                    self.myCanvas.ax.invert_xaxis()
        if (axis == 'y'):
            if (bChecked):
                if (not self.myCanvas.ax.yaxis_inverted()):
                    self.myCanvas.ax.invert_yaxis()
            else:
                if (self.myCanvas.ax.yaxis_inverted()):
                    self.myCanvas.ax.invert_yaxis()
        if (bDraw):
            try:
                self.myCanvas.draw()
            except:
                print "exception in toggleInversionAxis"            
            
    def toggleScale(self, axis, bChecked):
        if (self.chartType == 3 or self.chartType == 5): #polare, #stratigrafia
            bChecked = False
            if(self.currentEnvironment == ENVIROIMENTS['DESKTOP']):
                self.myUI.logxScale.setCheckState(False)
                self.logxScale = False
                #self.myUI.logyScale.setCheckState(False)
                #self.logyScale = False
            return
        if (axis == 'x' and self.chartType != 2): #date
            self.logxScale = bChecked
            if (self.myCanvas.ax == None):
                return
            if (bChecked):
                self.myCanvas.ax.set_xscale('symlog')
                self.myCanvas.ax.xaxis.get_major_formatter().labelOnlyBase = False
                '''
                tl = self.myCanvas.ax.get_xticks()
                labels = []
                for t in tl:
                    labels.append(t)
                self.myCanvas.ax.set_xticklabels(labels)
                '''
            else:
                self.myCanvas.ax.set_xscale('linear')
        if (axis == 'y'):
            self.logyScale = bChecked
            if (self.myCanvas.ax == None):
                return
            if (bChecked):
                self.myCanvas.ax.set_yscale('symlog')
                self.myCanvas.ax.yaxis.get_major_formatter().labelOnlyBase = False
                '''
                labels = []
                tl = self.myCanvas.ax.get_yticks()
                for t in tl:
                    labels.append(t)
                self.myCanvas.ax.set_yticklabels(labels)
                '''
            else:
                self.myCanvas.ax.set_yscale('linear')
        if(self.currentEnvironment == ENVIROIMENTS['DESKTOP']):
            self.redraw()
    
    def togglePoints(self, value):
        self.pointsViewer = value
        if (self.myLines == None):
            return
        if (value):
            for currLine in self.myLines:
                currLine.set_marker('o')
        else:
            for currLine in self.myLines:
                currLine.set_marker('None')
        if (isinstance(self.myCanvas, MplCanvas)):
            self.myCanvas.draw()
    
    def showGroupedChart(self, data, groupedField):
        self.myLines = []
        self.myBars = []
        self.groupList = []
        barLabels = []
        if (self.chartType == 5):
            maxLen = 0
            for o in data:
                if (maxLen < len(o)):
                    maxLen = len(o)
            self.chartSetup(maxLen)
        else:
            self.chartSetup(len(data))


        for idx, group in enumerate(data):
            ll = len(self.myLines)
            symbol = getSymbolByChartId(self.chartId)
            if (self.chartType == 2): # date
                self.xTicks = []
                self.myLines.append(self.myCanvas.ax.plot_date([],[],symbol)[0])
            if (self.chartType != 2 and self.chartType != 5 and self.chartType != 8):
                self.myLines.append(self.myCanvas.ax.plot([],[],symbol)[0])
                        
            xArr = []
            yArr = []
            groupName = None
            barLabels.append([])
            for elem in group:
                if (groupName == None and groupedField != None):
                    groupName = elem[groupedField]
                if (self.chartType == 2):
                    if (self.chartId == 14): #data e ora
                        self.xField =  self.xField.replace(' ', '')
                        xparams = self.xField.split(',')
                        lamiadata = elem[xparams[0]]
                        lamiaora = elem[xparams[1]]
                        arrOraMin = lamiaora.split('.')
                        hours1 = convert(arrOraMin[0],'int') or 0
                        minutes1 = convert(arrOraMin[1],'int') or 0
                        lamiadata = datetime.datetime(lamiadata.year, lamiadata.month, lamiadata.day, hours1, minutes1)
                        if (lamiadata.strftime('%d-%m-%Y %H:%M') not in self.xLabelsList):
                            self.xLabelsList.append(lamiadata.strftime('%d-%m-%Y %H:%M'))
                    else:
                        lamiadata = elem[self.xField]
                        if (lamiadata.strftime('%d-%m-%Y') not in self.xLabelsList):
                            self.xLabelsList.append(lamiadata.strftime('%d-%m-%Y'))
                    xArr.append(lamiadata)
                    if (lamiadata not in self.xTicks):
                        self.xTicks.append(lamiadata)                        
                else:
                    if (self.chartType == 3):
                        xArr.append(math.radians(elem[self.xField]))
                    else:
                        xArr.append(elem[self.xField])
                yArr.append(elem[self.yField])
                if (self.chartType == 5):
                    barLabels[len(barLabels)-1].append(str(elem['dstr_descrizione_new']))
                    if groupName not in self.groupList:
                        self.groupList.append(groupName)
            
            if (self.chartType == 5 and groupedField != None and groupName != None): # stratigrafia
                barColors = []
                currentBottom = 0
                iterableArr = range(len(yArr))
                self.xTicks = [0.25]
                self.yTicks = [0]
                for i in iterableArr:
                    barColors.append(self.myScalarmap.to_rgba(i))
                    self.yTicks.append(yArr[i])
                    self.myCanvas.ax.annotate(barLabels[idx][i], [0.6, currentBottom+((yArr[i]-currentBottom)/2)], fontproperties=matplotlib.font_manager.FontProperties(family='Tahoma', size=9))
                    self.myBars.append(self.myCanvas.ax.bar(idx, yArr[i]-currentBottom, width=0.5, bottom=currentBottom, color=barColors[i]))
                    self.myBars[len(self.myBars)-1].set_label(barLabels[idx][i]) # 8205878
                    currentBottom = yArr[i]

            elif self.chartType == 8:
                self.groupList = xArr
                self.myBars.append(self.myCanvas.ax.bar(np.arange(len(xArr)),yArr,color=self.myScalarmap.to_rgba(idx)))

            else:
                self.myLines[ll].set_xdata(xArr)
                self.myLines[ll].set_ydata(yArr)
                self.myLines[ll].set_color(self.myScalarmap.to_rgba(idx))
                self.myLines[ll].set_picker(2)
                self.myLines[ll].set_linewidth(self.lineWidth)
                if (groupName != None):
                    self.myLines[ll].set_label(groupName)
        
    def showChart(self, data):
        if (data == None or len(data) == 0):
            showMessage(self.currentEnvironment, 'critical','Errore','Nessun dato da visualizzare.\nSi prega di controllare i parametri selezionati')
            return
        
        if(self.currentEnvironment == ENVIROIMENTS['DESKTOP'] and not self.bDefaultParams):
            self.myUI.xMin.clear()
            self.myUI.xMax.clear()
            self.myUI.yMin.clear()
            self.myUI.yMax.clear()
        
        
        if (self.subgroupField is not None and len(data) == 1):
            editedData = []
            subgroups = OrderedDict()
            
            for sgn in self.selectedSubgroups:
                    if (not subgroups.has_key(sgn)):
                        subgroups[sgn] = []
                        
            for elem in data[0]:
                sgn = convert(elem[self.subgroupField],"string")
                
                if (subgroups.has_key(sgn)):
                    subgroups[sgn].append(elem)

            for key, value in subgroups.iteritems():
                editedData.append(value)
                
            self.showGroupedChart(editedData, self.subgroupField)
            #self.updateSubgroupList()
        else:
            if (self.subgroupField is not None and len(data) > 1):
                showMessage(self.currentEnvironment, 'error','Errore',"Attenzione, esistono piu' gruppi e sottogruppi contemporaneamente")
                return
            self.showGroupedChart(data, self.raggrupField)
        self.oncePlotted()
    
    def assignValues(self, tipoGrafico, nomeTabella, raggrup, subgroup, nomeCampoX, nomeCampoY, xLabel, yLabel, xMin, xMax, yMin, yMax):
        self.chartType = convert(tipoGrafico, "int")
        self.tableName = convert(nomeTabella, "string")
        self.raggrupField = convert(raggrup, "string")
        if (self.raggrupField == ''):
            self.raggrupField = None
        self.subgroupField = convert(subgroup, "string")
        if (self.subgroupField == ''):
            self.subgroupField = None
        self.xField = convert(nomeCampoX, "string")
        self.yField = convert(nomeCampoY, "string")
        self.xLabel = convert(xLabel, "string")
        self.yLabel = convert(yLabel, "string")
        if (self.chartType == 2):
            myType = "datenum"
        elif self.chartType == 8:
            myType = "datenumpy"
        else:
            myType = "float"

        self.xMin = convert(xMin, myType)
        self.xMax = convert(xMax, myType)
        myType = "float"
        self.yMin = convert(yMin, myType)
        self.yMax = convert(yMax, myType)
    
    # CUSTOM Mode
    def build(self, tipoGrafico, nomeTabella, nomeCampoX, nomeCampoY, xMin, xMax, yMin, yMax):
        self.assignValues(tipoGrafico, nomeTabella, None, None, nomeCampoX, nomeCampoY, nomeCampoX, nomeCampoY, xMin, xMax, yMin, yMax)
        myDB = db()
        data = myDB.getData2d(nomeTabella, nomeCampoX, nomeCampoY)
        myDB.disconnect()
        self.chartId = None
        self.chartLabel = nomeTabella
        self.showChart(data)        

    #DEFAULT Mode
    def buildByChart(self, chartId, raggrupId, xMin, xMax, yMin, yMax):
        myDb = db()
        obj = myDb.getChartById(chartId)
        if (obj[0]['sel_from'] == None):
            showMessage(self.currentEnvironment, "warning", "Attenzione", "Il grafico selezionato non risulta disponibile")
            return
        self.assignValues(obj[0]['id_tipo'], obj[0]['sel_from'], obj[0]['raggrup'], obj[0]['subgroup'], obj[0]['x'], obj[0]['y'], obj[0]['lab_x'], obj[0]['lab_y'], xMin, xMax, yMin, yMax)
        data = myDb.getDataByParams(chartId, raggrupId)
        self.chartId = chartId
        if (obj[0]['testalino'] != None):
            self.chartLabel = obj[0]['testalino']
        else:
            self.chartLabel = obj[0]['num_grafico']+" - "+obj[0]['nome_grafico']
        myDb.disconnect()
        self.showChart(data)
        self.insertCompanyLogo()
                    
    def saveImage(self,plotImgFilename,**kargs):
        '''
        create an image for chart and save it on disk
        '''
        pathToSave = os.path.dirname(os.path.abspath(__file__)) + self.saveImgDir
        if(self.currentEnvironment == ENVIROIMENTS['CLI']):
            pathToSave = ''
        #pathToSave = self.currentEnvironment == ENVIROIMENTS['CLI'] and '' or os.path.dirname(os.path.abspath(__file__)) 
        print pathToSave 
        self.myCanvas.savefig(pathToSave + plotImgFilename,**kargs)
