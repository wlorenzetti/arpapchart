# -*- coding: utf-8 -*-
'''
@author:     Walter Lorenzetti, Francesco Belllina
@copyright:  2013-2015 GIS3W. All rights reserved.
@license:    GPL2
@contact:    lorenzetti@gis3w.it
'''

import psycopg2
from models.user import User
from arpapchart.dbLib import db
from flask.ext.login import current_user

class Auth(object):

    def __init__(self):
        self.user = current_user
        if self.user.is_anonymous():
            self.user.username = self.username = 'AnonymousUserMixin'
        else:
            self.username = current_user.username
        self.acls = []

    def getUser(self):
        return self.user

    def login(self,username,password):
        try:
            con =psycopg2.connect(("dbname='%s' user='%s' host='%s' password='%s'")%(db.dbName,username,db.dbHost,password))
            if con:
                self.user = User(username)
                self.username = username
                con.close()
                return True
        except psycopg2.Error as e:
            return False

    def addACL(self,acl,**options):
        self.acls.append({'acl':acl,'options':options})
        return self

    def checkACL(self):
        res = False
        if self.user.is_anonymous():
            return res
        for a in self.acls:
            acli = a['acl'](self.user,**a['options'])
            if not res:
                res = acli.check()
        return res

class ACL(object):

    def __init__(self,user,**options):
        self.user = user
        self.options = options

    def check(self):
        pass

class ACLTypeChartUser(ACL):

    def check(self):
        myPGDB = db()
        users = myPGDB.getUsersForGroupChart(self.options['selection-group-type-chart'])
        if self.user.username in users:
            res = True
        else:
            res = False
        myPGDB.disconnect()
        return res

class ACLTypeChartRoles(ACL):

    def check(self):
        myPGDB = db()
        roles = myPGDB.getRolesForGroupChart(self.options['selection-group-type-chart'])
        if len(set(roles).intersection(self.user.getRoles())):
            res = True
        else:
            res = False
        myPGDB.disconnect()
        return res