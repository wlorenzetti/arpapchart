# -*- coding: utf-8 -*-
'''
@author:     Walter Lorenzetti
@copyright:  2013-2015 GIS3W. All rights reserved.
@license:    GPL2
@contact:    lorenzetti@gis3w.it
'''

from flask.ext.login import UserMixin
from arpapchart.dbLib import db

class User(UserMixin):

    def __init__(self,username = None,usersysid = None):
        self.username = username
        self.usersysid = usersysid
        self.roles = []
        self._load()

    def _load(self):
        myPGDB = db()
        if self.usersysid:
            u = myPGDB.query("SELECT usename, usesysid FROM pg_catalog.pg_user WHERE usesysid = "+str(self.usersysid)+";")
        else:
            u = myPGDB.query("SELECT usename, usesysid FROM pg_catalog.pg_user WHERE usename = '"+str(self.username)+"';")
        self.id = self.usersysid = u[0][1]
        self.username = u[0][0]
        myPGDB.disconnect()
        self._getRoles()

    def _getRoles(self):
        myPGDB = db()
        u = myPGDB.query("SELECT a.rolname from pg_catalog.pg_authid a where pg_has_role('"+str(self.username)+"',a.oid,'member');")
        for r in u:
            self.roles.append(r[0])
        myPGDB.disconnect()

    def getRoles(self):
        if len(self.roles) > 0:
            return self.roles
        else:
            self._getRoles()
            return self.roles

    @staticmethod
    def getById(id):
        return User(usersysid=id)
