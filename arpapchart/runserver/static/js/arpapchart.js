/*****************************************************
 * 
 *  Javascript methods for Arpapachart application
 * 
 *****************************************************/

/* Declaring namaspace*/
var AC = {
    actions: {},
    msgs: {},
    img: null,
    urlrootpath: '',
    selects: {},
    idsDefaultParameters: {
        'x_min':'',
        'x_max':'',
        'y_min':'',
        'y_max':'',
        'logaritmic_scale_x': false,
        'logaritmic_scale_y': false,
        'x_ticks':5,
        'y_ticks':5,
        'x_rotation_ticks':0,
        'invert_x':false,
        'invert_y':false,
        'line_width':3

    }
}

$.extend (AC.msgs,{
	ajaxError : function(res){
		$('#bt-generate-chart  span').toggleClass('fa-spin');
		var errMsg = 'An error is occoured, please try again. ';
		if(!_.isUndefined(arguments[1]))
			errMsg += arguments[1];
		$('#alertmsg').remove();
		$('#successmsg').remove();
		$('#messages').append(
				$('<div id="alertmsg">')
				.addClass('alert alert-danger')
				.html('<span>'+errMsg+'</span><button type="button" data-dismiss="alert" class="close" aria-hidden="true">&times;</button>')
				.alert()
		);
	},
	ajaxSuccess : function(res,msg){
		$('#alertmsg').remove();
		$('#successmsg').remove();
		$('#messages').append(
				$('<div id="successmsg">')
				.addClass('alert alert-success')
				.html('<span><strong>Graph generated</strong></span><button type="button" data-dismiss="alert" class="close" aria-hidden="true">&times;</button>')
				.alert()
		);
	}
});

$.extend (AC.actions, {
    showLoginForm: function(){
        $('#login_modal').modal()
            .on('shown.bs.modal',function(e){
                // populate hidden fields
                var modal = $(this);
                var hidden_type_group_chart = modal.find('input[type=hidden]');
                hidden_type_group_chart.val($('#selection-group-type-chart').val());
                $('#btnModalLoginLogin').click(function(e){
                    var data = {};
                    var fields = modal.find('form').serializeArray();
                    for (i in fields) {
                        data[fields[i]['name']] = fields[i]['value'];
                    }

                    AC.actions.ajaxLogin(data);
                });
            })
            .on('hidden.bs.modal', function (e) {
                $('#btnModalLoginLogin').unbind();
                //reset select typeo
                //$('#selection-group-type-chart').children().removeAttr('selected');
            });
    },
	resetFormValidationState : function(){
		$('.has-error').removeClass('has-error');
	},
    changeSelectMagic: function(){
        var isMultiGroupChart = $('#isMultiGroupChart');
        var idSelect = this.container.attr('id');
        var value = this.getValue();
        var doAjaxCall = true;
        console.log(idSelect);
        switch(idSelect){
            case "selection-group-type-chart":
                AC.actions.resetSelector(AC.selects['selection-number-chart-magic'],select.val() == '#' ? true : false);
                if(select.val() == '#')
                    AC.actions.resetSelectorMagic(AC.selects['id_raggr-magic'],true);
                AC.actions.resetSelectorMagic(AC.selects['sub_group-magic'],true);
		    break;
            case "id_raggr-magic":
                // change value on multiselect capability
                if(isMultiGroupChart.val() == 'true'){
                    doAjaxCall = false;
                    AC.actions.resetSelectorMagic(AC.selects['sub_group-magic'],true);
                }
                else
                {
                    value = value[0];
                    chartId = AC.selects['selection-number-chart-magic'].getValue()[0];
                    doAjaxCall = true;
                }
            break;
            case "selection-number-chart-magic":
                value = value[0];
                switch(value){
                    case "4":
                        // polar chart
                        // disabled logaritmic scale
                        $('#logaritmic_scale_y').attr('disabled','');
                        $('#logaritmic_scale_x').attr('disabled','');
                    break;
                }
                AC.actions.resetSelectorMagic(AC.selects['sub_group-magic'],true);

            break;
        }
        if(value == '' || _.isUndefined(value))
			doAjaxCall = false;
		if(doAjaxCall){
			if(idSelect == 'id_raggr-magic'){
				AC.actions.ajaxCall(idSelect,value,chartId);
			}
			else {
                AC.actions.ajaxCall(idSelect, value);
            }
		}


    },
	changeSelect: function(){
	var isMultiGroupChart = $('#isMultiGroupChart');
	var select =  $(this);
		// activate logaritmic scale
		$('#logaritmic_scale_y').removeAttr('disabled');
		$('#logaritmic_scale_x').removeAttr('disabled');
		var value = select.val();
		var idSelect = select.attr('id');
		var doAjaxCall = true;
		// switch between different input
		switch(idSelect){
            case "selection-group-type-chart":
            AC.actions.resetSelectorMagic(AC.selects['selection-number-chart-magic'],select.val() == '#' ? true : false);
            AC.actions.resetSelectorMagic(AC.selects['id_raggr-magic'],true);
			AC.actions.resetSelectorMagic(AC.selects['sub_group-magic'],true);
            break;
            case "selection-table":
                AC.actions.resetSelector($('#selection-x-col'));
                AC.actions.resetSelector($('#selection-y-col'));
            break;
		}
		if(value == '#')
			doAjaxCall = false;
		if(doAjaxCall){
			if(idSelect == 'id_raggr'){
				AC.actions.ajaxCall(idSelect,value,chartId);
			}
			else
			{
				AC.actions.ajaxCall(idSelect,value);
			}
			
		}
			
	},
    resetSelectorMagic: function(selector,disabled){
        selector.clear();
        if(disabled){
            selector.disable()
        }
        else if(selector.isDisabled()){
            selector.enable();
        }
	},
	resetSelector : function(selector,disabled){
		selector.empty();
		if(disabled){
			selector.prop('disabled',true);
		}else{
			if(selector.attr('disabled'))
				selector.prop('disabled',false);
		}
		//selector.append($('<option>').attr('value','').html('--'));
	},
	select_un: function(){
		// get the parent
		var select = $(this).parent().find('select');
		if($(this).hasClass('select-all')){
			select.children().prop('selected',true);
		} else {
			select.children().removeAttr('selected');
		}
		
	},
    select_un_magic: function(){
		// get the parent
		var select = $(this).parent().find('div');
		if($(this).hasClass('select-all')){
			AC.selects[select.attr('id')].setSelection(AC.selects[select.attr('id')].getData())
		} else {
			AC.selects[select.attr('id')].setSelection([])
		}

	},
    ajaxLogin: function(logindata) {
        $.ajax({
            url:"/"+AC.urlrootpath+"jx/login",
            method:'POST',
            data:logindata,
            success: function(res){
                if (res.login) {
                    $('#nav_login, #nav_logout').toggleClass('hidden');
                    $('#login_modal').modal('hide')
                    if(!_.isUndefined(res.referer_data)) {
                        AC.actions.ajaxCall(res.referer_data.idSelect,res.referer_data.selectValue);
                    }
                } else {
                    $('#loginErrMsg').addClass('alert alert-danger').html(res.errMsg)
;                }
            },
            error: function (res){

            }
        });
    },
	ajaxCall: function(idSelect,value){
        this.ajaxErrorMsgClear();
		if(value == '#')
			return true;
		var that = this;
		$.ajax({
			url:"/"+AC.urlrootpath+"jx/change",
			method: 'POST',
			data: arguments,
			success: function(res){
				if (!_.isUndefined(res.errMsg)) {
                    AC.msgs.ajaxError({},res.errMsg);
                    if(!_.isUndefined(res.resetForm) && res.resetForm) {
                        that.resetSelectorMagic(AC.selects['selection-number-chart-magic'],true);
                        that.resetSelectorMagic(AC.selects['id_raggr-magic'],true);
                        that.resetSelectorMagic(AC.selects['sub_group-magic'],true);
                    }

                    return true;
                }
				//iteration for change
				for(iddt in res.data){
					var objsel = res.data[iddt];
					var selector = $("#"+objsel.idselector);
                    if(!_.isUndefined(AC.selects[objsel.idselector])) {
                        that.resetSelectorMagic(AC.selects[objsel.idselector]);
                        AC.selects[objsel.idselector].setData(objsel.values)
                    } else {
                      that.resetSelector(selector);
                    }


					//update input hidden for chartype
					if(!_.isUndefined(objsel.idChartType))
						$('#idChartType').val(objsel.idChartType);
					if(!_.isUndefined(objsel.isMultiGroupChart))
						$('#isMultiGroupChart').val(objsel.isMultiGroupChart);

                    // update fields with default paramenters if they sett in res
                    if(!_.isNull(objsel.default_parameters)) {
                        that.setDefaultParameters(objsel.default_parameters);
                    } else {

                        that.resetDefaultParamenters()
                    }

					cont = 0;
					$.each(objsel.values,function(key,value){
						//specific case
						var toShow, toValue;
						switch(objsel.idselector)
						{
							case "selection-number-chart-magic":
								toValue = value['id'];
								toShow = value['name'];
								//if(cont == 0)
									//AC.actions.ajaxCall(objsel.idselector,toValue);

							break;
							
							default:									
								toShow = toValue = value
								 
						}

                        if(_.isUndefined(AC.selects[objsel.idselector])) {
                            selector.append($('<option>').attr('value', toValue).html(toShow));
                        }
						cont++;
					});
					
				}
			},
			error: function(res){
                if(res.status == 403){
                    that.resetSelectorMagic(AC.selects['selection-number-chart-magic'],true);
                    that.resetSelectorMagic(AC.selects['id_raggr-magic'],true);
                    that.resetSelectorMagic(AC.selects['sub_group-magic'],true);
                    AC.actions.showLoginForm();
                }
                else{
                    AC.actions.ajaxError(res);
                }

			}
		});
	},
    ajaxErrorMsgClear: function(){
        $('#messages').empty();
    },
	ajaxError: function(res){
		var errMsg = 'An error occoured try again'
		if(res !== jQuery.type())
			errMsg += res;
		$('#messages').html(errMsg);
	},
	ajaxSuccess: function(res){
		$('#messages').html('An error occoured try again');
	},
	validationData: function(){
		// Check section number chart
		var nChart = $('#selection-number-chart');
		var idChartType = AC.selects['selection-number-chart-magic'];
		var isMultiGroupChart = $('#isMultiGroupChart');
		var idRaggr = AC.selects['id_raggr-magic'];
		
//		switch(idChartType.val()){
//			case "25":
//			break;
//			default:
				//if is selected #id_raggr has a value and only
                /*
				if(idRaggr.getValue().length == 0)
				{
					AC.msgs.ajaxError({},'Sorry but you must select one id raggr. value');
					$(idRaggr).parent().toggleClass('has-error');
					return false;
				}
				*/
                if(idRaggr.getValue().length > 1 && isMultiGroupChart.val() == 'false')
			    {
					AC.msgs.ajaxError({},'Sorry but you must select only one id raggr. value');
					$(idRaggr).parent().toggleClass('has-error');
					return false;
			    }
					
				
//			break;
//			
//		}
		return true;
	},
    setDefaultParameters: function(defaultParameters) {
        console.log(defaultParameters);
        _.mapObject(defaultParameters,function(value,key) {
            var formField = $('#'+key);
            if(formField.length != 0) {
                if(_.isBoolean(value)) {
                    formField.prop("checked",value);
                } else {
                    formField.val(value);
                }
            }
        });
    },

    resetDefaultParamenters: function() {
        this.setDefaultParameters(AC.idsDefaultParameters);
    }
});

$(document).ready(function(){
    /**
     * Initialize bootstrap plugins
     */

    AC.selects['selection-number-chart-magic']=$('#selection-number-chart-magic').magicSuggest({
        name: 'number_chart',
        placeholder: 'Select...',
        allowFreeEntries: false,
        maxSelection:1,
        disabled:true
    });

    AC.selects['id_raggr-magic']=$('#id_raggr-magic').magicSuggest({
        name: 'id_raggr',
        placeholder: 'Select...',
        allowFreeEntries: false,
        disabled:true
    });

    AC.selects['sub_group-magic']=$('#sub_group-magic').magicSuggest({
        name: 'sub_group',
        placeholder: 'Select...',
        allowFreeEntries: false,
        disabled:true
    });

    $(AC.selects['selection-number-chart-magic']).on('selectionchange',AC.actions.changeSelectMagic);
    $(AC.selects['id_raggr-magic']).on('selectionchange',AC.actions.changeSelectMagic);

	/**
	 * Trigger on button click for chart generation
	 */
	$('#bt-generate-chart').click(function(){
		$('#bt-generate-chart  span').toggleClass('fa-spin');
		AC.actions.resetFormValidationState();
		// First check validation data
		if(AC.actions.validationData()) {
            data = $(':input').serializeArray();
            // rename e refactoring for post

            $.ajax({
				url:"/"+AC.urlrootpath+"jx/buildchart",
				method: 'POST',
				data: data,
				success: function(res){
					AC.msgs.ajaxSuccess(res);
					// activate download button
					$('#bt-download-chart').removeClass('disabled');
					$('#image-chart').attr('src','/'+AC.urlrootpath+'static/img/cache/' + res.data.src + '?' + Math.random());
					$('#bt-generate-chart  span').toggleClass('fa-spin');
					// if there is qther res:
					if(_.has(res.data,'sub_group_values')){
						// fill sub_group select
						var sub_group = $('#sub_group');
						sub_group.empty();
						$.each(res.data.sub_group_values, function(k,v){
							sub_group.removeAttr('disabled').append($('<option>').val(v).text(v));
						});
						// all options selected
						//sub_group.children().prop('selected',true);


					}
				},
				error: function(res){
					// deactivate download button
					$('#bt-download-chart').addClass('disabled');
					AC.msgs.ajaxError(res);
				},
			});
        }

		
	});
	
	/**
	 * For downloading chart image
	 */
	$('#bt-download-chart').click(function(e){
			e.preventDefault(); // this will prevent the browser to redirect to the href
			window.location.href = '/'+AC.urlrootpath+'download/chart' + '?' + Math.random();
	});
	
	$('#selection-table,#selection-type-chart,#selection-group-type-chart,#id_raggr').change(AC.actions.changeSelect);
	
	
	// link clicke selceted e unselected button
	$('.select-all, .unselect-all').click(AC.actions.select_un_magic);


});