# -*- coding: utf-8 -*-
'''
@author:     Walter Lorenzetti, Francesco Belllina
@copyright:  2013-2015 GIS3W. All rights reserved.
@license:    GPL2
@contact:    lorenzetti@gis3w.it
'''

from arpapchart.globals import *
from arpapchart.utils import *
from arpapchart.dbLib import db
from flask import *
import matplotlib.pyplot as plt
from matplotlib.backends.backend_agg import FigureCanvasAgg
from arpapchart.chartLib import chart
from flask.globals import request, session
import argparse
import os, sys, re
import StringIO
import logging
from logging.handlers import RotatingFileHandler
from logging import Formatter
import psycopg2
from flask.ext.login import LoginManager,login_user,logout_user,login_required, current_user
from flask.globals import current_app
from models.user import User
from auth import Auth, ACLTypeChartRoles,ACLTypeChartUser

webApp = Flask(__name__)
#if not exixts touch it
fileLog = os.path.dirname(os.path.realpath(__file__))+'/'+WEB_DEFAULT_VALUES['LOG_FILE']
if not os.path.isfile(fileLog):
    open(fileLog,'w+').close()
    os.chmod(fileLog,0777)
handler = RotatingFileHandler(fileLog, maxBytes=10000000, backupCount=1)
format = Formatter(fmt='%(asctime)s [%(levelname)s]: %(message)s',datefmt='%m/%d/%Y %I:%M:%S %p')
handler.setFormatter(format)
webApp.logger.addHandler(handler)

login_manager = LoginManager()
login_manager.init_app(webApp)


Flask.secret_key = WEB_SESSION_SECRETKEY

def getPostData():
    '''
    get Data from Http POST send
    '''
    data={}
    for k in request.form:
        data[k] = request.form[k]
    return data

def getGetData():
    '''
    get Data from Http GET send
    '''
    data={}
    for k in request.args:
        data[k] = request.args[k]
    return data

def _ACL(idSelect,selectValue):
    myPGDB = db()
    isUnderAuthorization = myPGDB.isGroupChartUnderAuthorization(selectValue)
    myPGDB.disconnect()
    options = {'selection-group-type-chart':selectValue}
    a = Auth().addACL(ACLTypeChartUser,**options).addACL(ACLTypeChartRoles,**options)
    if isUnderAuthorization:
        if not current_user.is_authenticated():
            status_403 = True
            if request.path == '/getchart':
                postData = getGetData()
                username = postData.get('username',None)
                password = postData.get('password',None)
                if username and password and a.login(username,password):
                    if a.checkACL():
                        status_403 = False
            if status_403:
                #save in session referer
                session['login_referer_data'] ={'idSelect':idSelect,'selectValue':selectValue}
                return Response(status=403)
        else:
            if not a.checkACL():
                return jsonify({'errMsg':'You don\'t have permission on this group type chart','resetForm':True})
    return True

def errorChartData(msg=''):
    return render_template('500.html',errMsg='Sorry a problem is occurred: Not good data from Database for to build chart! '+msg),500


webApp.debug = WEBDEBUG

@webApp.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

@webApp.errorhandler(500)
def page_error(e):
    return render_template('500.html'), 500

@webApp.context_processor
def inject_globals():
    return dict(defaults=WEB_DEFAULT_VALUES, version=VERSION, py2jsdata=dict(urlrootpath=WEB_URLROOTPATH), urlRoot=WEB_URLROOTPATH)

@webApp.route("/jx/login",methods=['POST'])
def ajaxLogin():
    postData = getPostData()
    if (not 'username' in postData) or (not 'password' in postData):
        errMsg = "Login error with username='"+postData['username']+"' not username or password supplied"
        webApp.logger.error(errMsg)
        res = {'login':False,'errMsg':errMsg}
    else:
        a = Auth().addACL(ACLTypeChartUser,**postData).addACL(ACLTypeChartRoles,**postData)
        if a.login(postData['username'],postData['password']):
            #before we check ACL policy
            if a.checkACL():
                login_user(a.getUser())
                res = {'login':True, 'referer_data':session['login_referer_data']}
            else:
                res = {'login':False,'errMsg':'You don\'t have permission on this group type chart'}
        else:
            errMsg = "Login error with username='"+postData['username']+"'"
            webApp.logger.error(errMsg)
            res = {'login':False,'errMsg':errMsg}
    return jsonify(res)



@webApp.route("/login", methods=["GET", "POST"])
def login():
    errMsg = False
    if request.method == 'POST':
        postData = getPostData()
        if (not 'username' in postData) or (not 'password' in postData):
            errMsg = True
            webApp.logger.error("Login error with username='"+postData['username']+"' and password='"+postData['username']+"'")
        else:
            a = Auth()
            if a.login(postData['username'],postData['password']):
                login_user(a.getUser())
                if 'login_referer' in session:
                    return redirect(session['login_referer'])
                else:
                    return redirect(url_for("main"))
            else:
                errMsg = True
                webApp.logger.error("Login error with username='"+postData['username']+"' and password='"+postData['username']+"'")
    return render_template("login.html", errMsg=errMsg, login_page=True)


@webApp.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for("main"))

@login_manager.user_loader
def load_user(userid):
    return User.getById(userid)


@webApp.route("/",methods=['GET', 'POST'])
def main():
    '''
    Home(Main) web page controller
    '''

    #for start application we start with default value for charttype
    #get group chart
    myPGDB = db()
    groupsChart = myPGDB.getChartGroups()
    myPGDB.disconnect()
    #chartTypeDefault = myPGDB.getChartsByGroup(groupsChart[0][0])
    #we check if is a mutigroup chart
    #isMultiGroupChart = myPGDB.isMultiGroupChart(chartTypeDefault[0]['id_graf'])
    return render_template('form.html', groupsChart=groupsChart)

@webApp.route("/jx/change",methods=['GET', 'POST'])
def change():
    '''
    Ajax change select box controller
    '''
    myPGDB = db()
    idSelect = request.form['0']
    selectValue = request.form['1']
    if '2' in request.form:
        chartId = request.form['2'] 

    result = []
    if idSelect == 'selection-type-chart':
        #get db tables:
        tNames = myPGDB.getTables();
        tNamest = [];
        idSelector = 'selection-table'
        for v in tNames:
            tNamest += [v[0]];
        result += [{'idselector':idSelector,'values':tNamest}] 
        
    elif idSelect == 'selection-table':
       #get db tables:
        tFields = myPGDB.getTableFields(selectValue)
        tFieldst = [];
        for v in tFields:
            tFieldst += [v[0]];
            
        result += [{'idselector':'selection-x-col','values':tFieldst},{'idselector':'selection-y-col','values':tFieldst}] 
        
    elif idSelect == 'selection-group-type-chart':
        '''
        Select table on form
        '''
        #check if group chart type id under authentication
        acl = _ACL(idSelect,selectValue)
        if acl != True:
            return acl

       #get numbered type chart:
        chartTypeDefault = myPGDB.getChartsByGroup(selectValue)
        tNCharts = [];
        for v in chartTypeDefault:
            tNCharts += [{'id':v['id_graf'],'name':v['num_grafico'] + ' - ' + v['nome_grafico']}]
        result += [{'idselector':'selection-number-chart-magic','values':tNCharts}]
        
    elif idSelect == 'selection-number-chart-magic':
        tNCharts = []
        valoriCustom = myPGDB.getAllRaggrupValuesUsingCustomQuery(selectValue)
        if valoriCustom:
            for v in valoriCustom:
                tNCharts += [{'id':v,'name':v}]
        else:
            availableRaggrupId = myPGDB.getRaggrupIdsByChart(selectValue)
            valori = []
            if len(availableRaggrupId) > 0:
                valori = myPGDB.getAllRaggrupValues(selectValue)
                for v in valori:
                    tNCharts += [{'id':v,'name':v}]


        chartType = myPGDB.getChartById(selectValue)
        try:
            default_parameters = None
            if chartType[0]['default_parameters']:
                default_parameters = json.loads(chartType[0]['default_parameters'])
        except Exception as e:
            webApp.logger.error(e.message)
        isMultiGroupChart = myPGDB.isMultiGroupChart(selectValue)

        result += [{'idselector':'id_raggr-magic','values':tNCharts,'idChartType':chartType[0]['id_tipo'],'isMultiGroupChart':isMultiGroupChart,'default_parameters':default_parameters}]
        
    elif idSelect == 'id_raggr-magic':
        raggrupId = "'"+ selectValue +"'"
        subGroups = myPGDB.getAllSubgroupValues(chartId, raggrupId)
        tNCharts = []
        if subGroups:
            for s in subGroups:
                tNCharts += [{'id':s,'name':s}]
        result += [{'idselector':'sub_group-magic','values':tNCharts}]
    else:
        pass
    myPGDB.disconnect()
    return jsonify(data=result)

@webApp.route("/getchart",methods=['GET'])
def getChart():
    '''
    Send PLOT image by parmans controller
    '''

    return buildChart()

    



@webApp.route("/jx/buildchart",methods=['POST'])
def buildChart():
        '''
        Main plot generation controller
        '''
        # get POST data
        modeCall = 'GET' if request.path == '/getchart' else 'POST'
        postData = modeCall == 'POST' and getPostData() or getGetData()
        
        modeChart = postData.get('mode_chart','default')
        chartType = postData.get('type_chart',None)
        tab = postData.get('table',None)
        x = postData.get('x_col',None)
        y = postData.get('y_col',None)    
        xMin = postData.get('x_min',None)
        xMax = postData.get('x_max',None)
        yMin = postData.get('y_min',None)
        yMax = postData.get('y_max',None)



        groupChart = postData.get('group_type_chart',None)
        if modeCall == 'POST':
            id_raggr = request.form.getlist('id_raggr[]')
            subGroup = request.form.getlist('sub_group[]')
            numberChart = convert(postData.get('number_chart[]',None),'int')
            
        else:
            myPGDB = db()
            res = myPGDB.getChartByCode(postData.get('number_chart',None))
            if not res and not WEBDEBUG:
                return errorChartData()
            numberChart = res[0][0]
            groupChart = myPGDB.getGroupChartByNumberChart(postData.get('number_chart',None))
            if not groupChart and not WEBDEBUG:
                return errorChartData()
            id_raggr = postData.get('id_raggr',None).split(',')
            id_raggr_for_numberchart = myPGDB.getAllReggrupValuesByNumberChart(postData.get('number_chart',None))
            if not set(id_raggr) < set(id_raggr_for_numberchart):
                return errorChartData('Spiacente ma i gruppi indicati {} non apprtengono al grafico {}'.format(', '.join(id_raggr),postData.get('number_chart',None)))
            subGroup = postData.get('sub_group',None)
            if subGroup:
                if subGroup == 'all' :
                    subGroup = myPGDB.getAllSubgroupValues(numberChart, "'"+ id_raggr[0] +"'")
                    if not subGroup and not WEBDEBUG:
                        return errorChartData()
                else:
                    subGroup = subGroup.split(',')
            myPGDB.disconnect()

        #che auth and ACL
        if groupChart:
            acl = _ACL('selection-type-chart',groupChart)
            if acl != True:
                if request.path == '/getchart' and acl.status_code == 403:
                    session['login_referer'] = request.url
                    return redirect(url_for("login"))
                else:
                    return acl
        else:
            return Response(status=500)

        
        idRaggr = '\'' + '\',\''.join(id_raggr) + '\''
        if not idRaggr:
            idRaggr = None
            
            
        showPoints = postData.get('show_points',None)
        showGrid = postData.get('show_grid',None)
        logaritmicScaleX = postData.get('logaritmic_scale_x',None)
        logaritmicScaleY = postData.get('logaritmic_scale_y',None)
        
        invertX = postData.get('invert_x',None)
        invertY = postData.get('invert_y',None)
        
        wpixels = postData.get('chart_width',WEB_DEFAULT_VALUES['CHART_WIDTH_PX'])
        hpixels = postData.get('chart_height',WEB_DEFAULT_VALUES['CHART_HEIGHT_PX'])
            
        winch = int(wpixels) / WEB_DEFAULT_VALUES['CHART_DPI']
        hinch = int(hpixels) / WEB_DEFAULT_VALUES['CHART_DPI']
        
        # build ax and fig properties
        # for chartLib object
        plt.subplots()
        plt.fig, plt.ax = plt.subplots()
        plt.fig.set_dpi(WEB_DEFAULT_VALUES['CHART_DPI'])
        plt.fig.set_tight_layout(True)
        plt.fig.set_size_inches(winch,hinch)
        myChart = chart(plt)
        

        myChart.xGrid = postData.get('x_ticks',WEB_DEFAULT_VALUES['X_N_TICKS'])
        myChart.yGrid = postData.get('y_ticks',WEB_DEFAULT_VALUES['Y_N_TICKS'])

        myChart.xRotationTicks = postData.get('x_rotation_ticks') if postData.get('x_rotation_ticks') else WEB_DEFAULT_VALUES['X_ROTATION_TICKS']

        myChart.lineWidth = postData.get('line_width') if postData.get('line_width') else WEB_DEFAULT_VALUES['LINE_WIDTH']

        
        #set legend
        myChart.legendLoc = 2
        
        #set evniroiments
        myChart.currentEnvironment = ENVIROIMENTS['WEB']
        
        if showPoints == '1':
            myChart.pointsViewer = True

        if showGrid == '1':
            myChart.bGrid = True
        else:
            myChart.bGrid = False
            
        if logaritmicScaleX == '1':
            myChart.logxScale = True
            
        if logaritmicScaleY == '1':
            myChart.logyScale = True
            
        #chart with subGroud data:
        if subGroup:
            onSelectedSubgroup(myChart,subGroup)

        try:
            #build the chart
            if modeChart == 'default':
                myChart.buildByChart(numberChart, idRaggr, xMin, xMax, yMin, yMax)
            else:
                myChart.build(chartType, tab, x, y)
        except Exception as ex:
            return errorChartData(str(ex))

            
        #invert axis
        if invertX == '1':
            myChart.toggleInversionAxis('x', True, False)
        
        if invertY == '1':
            myChart.toggleInversionAxis('y', True, False)

        

        #myChart.myCanvas.get_canvas_width_height()
        if myChart.chartType != 5 :
            myChart.updateLegend(myChart.myLines)
            if len(myChart.myLines) > 0 and myChart.myLegend:
                myChart.myLegend.set_bbox_to_anchor((1,0.5,0.5,0.5))
           
        
        #get generated image
        #check for non ascii character from db
        if len(id_raggr) > 0:
            plotImgFilename = ''.join([i if ord(i) < 128 else '-' for i in '_'.join(id_raggr)])
        else:
            plotImgFilename = 'plot_{}'.format(numberChart)
        #replace blankspace and - whit _ int filename string
        plotImgFilename = re.sub(r'\ |-|\/','_',plotImgFilename) + '.png'
        session['plotImgFilename'] = plotImgFilename
        kargs = {'bbox_inches':'tight'}
        if myChart.chartType != 5 and len(myChart.myLines) > 0 and myChart.myLegend :
            kargs['bbox_extra_artists'] = (myChart.myLegend,)
        #update legend
 
        if modeCall == 'POST':
            myChart.saveImage(plotImgFilename, **kargs)
            plt.close();
            #return results
            result = {'src':plotImgFilename}

            # we just add other data if necessary:
            return jsonify(data=result)
        else:
            png_output = StringIO.StringIO()
            myChart.myCanvas.savefig(png_output,**kargs)
            plt.close();
            response=make_response(png_output.getvalue())
            response.headers['Content-Type'] = 'image/png'
            return response
        #close the plot instantion

    
def onSelectedSubgroup(myChart,subGroup):
    '''
    Set sub group items in mychart object if submint
    '''
    myChart.selectedSubgroups = []
    for sb in subGroup:
        myChart.selectedSubgroups.append(sb)

    #myChart.redraw()
    
@webApp.route("/download/chart")
def downloadChart():
    '''
    Download plot controller
    '''
    pathToSave = os.path.dirname(os.path.abspath(__file__))
    return send_from_directory(pathToSave + '/static/img/cache/', session['plotImgFilename'],as_attachment = True)

@webApp.route("/credits")
def credits():
    '''
    Credits page controller
    '''
    return render_template('credits.html')
    
class webRun(argparse.Action):
    '''
    Main class run web application
    '''
    def __call__(self, parser, namespace, values, option_string=None):
        webApp.run(port = values)
    