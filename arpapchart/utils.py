# -*- coding: utf-8 -*-
'''
@author:     Walter Lorenzetti, Francesco Belllina
@copyright:  2013-2015 GIS3W. All rights reserved.
@license:    GPL2
@contact:    lorenzetti@gis3w.it
'''

from arpapchart.globals import *
from arpapchart.desktop.mplwidget import *
from datetime import *
from matplotlib import dates
import numpy as np

def isset(variable):
    variable = str(variable)
    return variable in locals() or variable in globals()

def convert(valore, tipo):
    if (valore is None):
        return None
    a = None
    try:
        if tipo == "datenum" or tipo == "datenumpy":
            myDateArr=str(valore).split("/")
            day = int(myDateArr[0])
            month = int(myDateArr[1])
            year = int(myDateArr[2])
            if tipo == "datenum":
                a = datetime(year,month,day)
                a = dates.date2num(a)
            else:
                a = np.datetime64('{}-{}-{} 00:00:00'.format(myDateArr[2],myDateArr[1],myDateArr[0]))
        if (tipo == "float"):
            a = float(valore)
        if (tipo == "int"):
            a = int(valore)
        if (tipo == "string"):
            if isinstance(valore, unicode):
                a = valore.encode("utf-8")
            else:
                a = str(valore)
    except Exception as e:
        #print e
        return None
    else:
        return a
    
def showMessage(environment, myType, myTitle, myText):
    if (environment == ENVIROIMENTS['DESKTOP']):
        try:
            msgBox = QtGui.QMessageBox()
            if (myType=="warning"):
                QtGui.QMessageBox.warning( msgBox,myTitle,myText)
            if (myType=="information"):
                QtGui.QMessageBox.information( msgBox,myTitle,myText)
            if (myType=="critical"):
                QtGui.QMessageBox.critical( msgBox,myTitle,myText)
        except:
            print myTitle+" - "+myText
    else:
        print myTitle+" - "+myText
        raise Exception(myTitle+" - "+myText)

def getSymbolByChartId(chartId):
    pointsNeed = [14]
    if (chartId in pointsNeed):
        return 'o'
    else:
        return '-'
'''
'-'    solid line style
'--'    dashed line style
'-.'    dash-dot line style
':'    dotted line style
'.'    point marker
','    pixel marker
'o'    circle marker
'v'    triangle_down marker
'^'    triangle_up marker
'<'    triangle_left marker
'>'    triangle_right marker
'1'    tri_down marker
'2'    tri_up marker
'3'    tri_left marker
'4'    tri_right marker
's'    square marker
'p'    pentagon marker
'*'    star marker
'h'    hexagon1 marker
'H'    hexagon2 marker
'+'    plus marker
'x'    x marker
'D'    diamond marker
'd'    thin_diamond marker
'|'    vline marker
'_'    hline marker
'''
   
'''
class colorConverter(): 
    _NUMERALS = '0123456789abcdefABCDEF'
    _HEXDEC = {v: int(v, 16) for v in (x+y for x in _NUMERALS for y in _NUMERALS)}
    LOWERCASE, UPPERCASE = 'x', 'X'
    
    def rgb(self, triplet):
        return (self._HEXDEC[triplet[0:2]], self._HEXDEC[triplet[2:4]], self._HEXDEC[triplet[4:6]])
    
    def triplet(self, rgb, lettercase=LOWERCASE):
        return format((rgb[0]<<16 | rgb[1]<<8 | rgb[2]), '06'+lettercase)
'''


