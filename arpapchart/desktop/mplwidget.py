# -*- coding: utf-8 -*-
'''
@author:     Walter Lorenzetti, Francesco Belllina
@copyright:  2013-2015 GIS3W. All rights reserved.
@license:    GPL2
@contact:    lorenzetti@gis3w.it
'''

from PyQt4 import QtGui
from matplotlib.backends.backend_qt4agg import *
from matplotlib.figure import *
 
class MplCanvas(FigureCanvasQTAgg):
 
    def __init__(self):
        self.fig = Figure()
        self.ax = None
 
        FigureCanvasQTAgg.__init__(self, self.fig)
        FigureCanvasQTAgg.setSizePolicy(self, QtGui.QSizePolicy.Expanding,QtGui.QSizePolicy.Expanding)
        FigureCanvasQTAgg.updateGeometry(self)

    def set_filename_default_image(self, fileName):
        self.filename_default_image = fileName

    def get_default_filename(self):
        if hasattr(self,'filename_default_image'):
            return self.filename_default_image + '.' + self.get_default_filetype()
        else:
            return FigureCanvasQTAgg.get_default_filename(self)
 
 
class MplWidget(QtGui.QWidget):
    def __init__(self, parent = None):
        main_frame = QtGui.QWidget.__init__(self, parent)
        self.canvas = MplCanvas()
        self.mpl_toolbar = NavigationToolbar2QT(self.canvas, main_frame)
        self.vbl = QtGui.QVBoxLayout()
        self.vbl.addWidget(self.mpl_toolbar)
        self.vbl.addWidget(self.canvas)        
        self.setLayout(self.vbl)
'''       
class MplLegendWidget(QtGui.QWidget):
    def __init__(self, parent = None):
        main_frame = QtGui.QWidget.__init__(self, parent)
        self.canvas = MplCanvas()
        self.vbl = QtGui.QVBoxLayout()
        self.vbl.addWidget(self.canvas)        
        self.setLayout(self.vbl)
'''