# -*- coding: utf-8 -*-
'''
@author:     Walter Lorenzetti, Francesco Belllina
@copyright:  2013-2015 GIS3W. All rights reserved.
@license:    GPL2
@contact:    lorenzetti@gis3w.it
'''

import sys
import json
from arpapchart.globals import *
from arpapchart.dbLib import db
from arpapchart.utils import *
from ui_chartdialog import *
from arpapchart.chartLib import chart
from arpapchart.desktop.mplwidget import *
#from arpapchart.desktop.FilteringComboBox import *
import pprint


class APP(QtGui.QMainWindow):
    myPGDB = None
    myChart = None
    bRaggrup = False
    args = None
    bArgs = False
    bCmdLine = False
    raggrupList = None
    
    def findInQListWidget(self, qlw, what):
        allItems = qlw
        totItems = allItems.count()
        myRange = xrange(totItems)
        filteredItems = qlw.findItems(what, QtCore.Qt.MatchContains)
        for index in myRange:
            item = allItems.item(index)
            item.setHidden(not (item in filteredItems))
        return
    
    def onScaleCheck(self, axis):
        if (axis == 'x'):
            self.myChart.toggleScale(axis, self.ui.logxScale.isChecked())
            self.myChart.setLocator(axis, self.ui.xgrid.text(), False)
        if (axis == 'y'):
            self.myChart.toggleScale(axis, self.ui.logyScale.isChecked())
            self.myChart.setLocator(axis, self.ui.ygrid.text(), False)
        self.myChart.setLimits(True)
        bX = self.ui.invert_xaxis.isChecked()
        bY = self.ui.invert_yaxis.isChecked()
        self.myChart.toggleInversionAxis('x', bX, True)
        self.myChart.toggleInversionAxis('y', bY, True)
    
    def onLimitChange(self, axis):
        self.myChart.setLimits(False)
        if (axis == 'x'):
            self.myChart.setLocator('x', self.ui.xgrid.text(), False)
        if (axis == 'y'):
            self.myChart.setLocator('y', self.ui.ygrid.text(), False)
        self.myChart.redraw()
    
    '''
    def onSelectedSubgroup(self):
        sis = self.ui.subgroupId.selectedItems()
        if (len(self.myChart.selectedSubgroups)>0):
            for pss in self.myChart.selectedSubgroups:
                if pss in sis:
                    if (self.myChart.myLines):
                        for line in self.myChart.myLines:
                            label = line.get_label()
                            if (label == pss):
                                line.set_visible(True)
                                line.set_picker(True)
                                break
                    else:
                        self.onGenerateClick()
                else:
                    self.myChart.selectedSubgroups.remove(pss)
            for si in sis:
                if (si not in self.myChart.selectedSubgroups):
                    self.myChart.myLines.append()
        else:
            self.onGenerateClick()
    '''
        
    def onSelectedSubgroup(self):
        self.myChart.selectedSubgroups = []
        for si in self.ui.subgroupId.selectedItems():
            t = convert(si.text(),"string")
            self.myChart.selectedSubgroups.append(t)
        if (len(self.myChart.selectedSubgroups) > 0):
            self.onGenerateClick()
        else:
            self.clearElementoSelezionato()
            for line in self.myChart.myLines:
                line.set_visible(False)
                line.set_picker(False)
            self.myChart.updateLegend([])
            self.myChart.redraw()
            
        '''self.clearElementoSelezionato()
        addedLines = []
        for line in self.myChart.myLines:
            label = line.get_label()
            bPresent = (label in self.myChart.selectedSubgroups)
            line.set_visible(bPresent)
            line.set_picker(bPresent)
            if (bPresent):
                addedLines.append(line)
        self.myChart.updateLegend(addedLines)
        self.myChart.redraw()'''
            
    def getDefaultArgs(self, myStr): #default parameters stored in db
        myObj = json.loads(myStr)
        
        if len( myObj.keys()) > 0:
            for key in myObj.keys():
                value = myObj[key]
                if (key == 'logaritmic_scale_x' and not self.args['logx']):
                    self.args['logx'] = value
                    continue;
                if (key == 'logaritmic_scale_y' and not self.args['logy']):
                    self.args['logy'] = value
                    continue;
                if (key == 'x_min' and self.args['minx'] is None):
                    self.args['minx'] = convert(value, "string")
                    continue;
                if (key == 'x_max' and self.args['maxx'] is None):
                    self.args['maxx'] = convert(value, "string")
                    continue;
                if (key == 'y_min' and self.args['miny'] is None):
                    self.args['miny'] = convert(value, "string")
                    continue;
                if (key == 'y_max' and self.args['maxy'] is None):
                    self.args['maxy'] = convert(value, "string")
                    continue;
                if (key == 'invert_x' and not self.args['invx']):
                    self.args['invx'] = value
                    continue;
                if (key == 'invert_y' and not self.args['invy']):
                    self.args['invy'] = value
                    continue;
                if (key == 'x_ticks' and self.args['xticks'] is None):
                    self.args['xticks'] = convert(value, "int")
                    continue;
                if (key == 'y_ticks' and self.args['yticks'] is None):
                    self.args['yticks'] = convert(value, "int")
                    continue;
                if (key == 'x_rotation_ticks' and self.args['x_rotation_ticks'] is None):
                    self.args['x_rotation_ticks'] = convert(value, "int")
                    continue;
                if (key == 'line_width' and self.args['line_width'] is None):
                    self.args['line_width'] = convert(value, "int")
                    continue;
                if (key == 'show_points' and self.args['spt'] is None):
                    self.args['spt'] = value
                    continue;
    
    def onChartChange(self):
        self.updateRaggrupId()
        currentData = self.ui.chart.itemData(self.ui.chart.currentIndex()).toInt()[0]
        if (currentData > 0 and self.myPGDB.isMultiGroupChart(currentData)):
            self.ui.raggrupId.setSelectionMode(QtGui.QAbstractItemView.MultiSelection)
        else:
            self.ui.raggrupId.setSelectionMode(QtGui.QAbstractItemView.SingleSelection)
        self.ui.subgroupId.clear()
        self.ui.groupFinder.clear()
        self.ui.subgroupFinder.clear()
        self.ui.subgroupId.setEnabled(False)
        self.ui.selAll_subgroup.setEnabled(False)
        self.ui.cancelAll_subgroup.setEnabled(False)
        self.clearElementoSelezionato()
        if (self.myChart.myCanvas is not None and self.myChart.myCanvas.ax is not None):
            self.myChart.myCanvas.fig.delaxes(self.myChart.myCanvas.ax)
            self.myChart.myCanvas.ax = None
        self.myChart.redraw()
        
        
        defPar = self.myPGDB.getDefaultParameters(currentData)
        self.resetInputArgs()
        if (defPar):
            self.myChart.bDefaultParams = True
            self.getDefaultArgs(defPar)
        else:
            self.myChart.bDefaultParams = False
        
        self.bArgs = True
        #self.bCmdLine = True
        self.lineWidthInitializer()
        self.limitInitializer()
        self.logAxisInitializer()
        self.pointInitializer()
        self.inversionAxisInitializer()
        self.gridInitializer()
        self.tickInitializer()
        self.bArgs = False
        
        if (not self.bRaggrup and currentData>0):
            self.onGenerateClick()
            
    def onSelectedGroup(self):
        self.myChart.selectedGroups = []
        groups = self.ui.raggrupId.selectedItems()
        for g in groups:
            vvv = convert(g.text(), "string")
            self.myChart.selectedGroups.append(vvv)
        self.myChart.myCanvas.set_filename_default_image('_'.join(self.myChart.selectedGroups))

    
    def listSubgroups(self):
        self.onSelectedGroup()
        self.myChart.subgroupList = []
        groups = self.ui.raggrupId.selectedItems()
        if (len(groups) == 0):
            self.updateSubgroupList()
            self.onGenerateClick()
            return
        
        chartId = self.ui.chart.itemData(self.ui.chart.currentIndex()).toInt()[0]
        raggrupId = convert(groups[0].text(), "string")
        if raggrupId is not None:
            raggrupId = "'"+raggrupId+"'"
        
        subgroups = {}
        subgroupsFromCustomQuery = self.myPGDB.getAllSubgroupValues(chartId, raggrupId)
        
        if (subgroupsFromCustomQuery is not None):
            for sgn in subgroupsFromCustomQuery:
                if (not subgroups.has_key(sgn)):
                    subgroups[sgn] = []
                    self.myChart.subgroupList.append(sgn)
            self.updateSubgroupList()
        else:
            self.onGenerateClick()
        
    def updateSubgroupList(self):
        if (self.myChart.currentEnvironment == ENVIROIMENTS['DESKTOP']):
            subgroups = self.myChart.subgroupList
            self.ui.subgroupId.clear()
            if len(subgroups) > 0:
                subgroups.sort()
                for sg in subgroups:
                    self.ui.subgroupId.addItem(str(sg))
                #self.toggleAllSubgroups(True)
                self.ui.subgroupId.setEnabled(True)
                self.ui.selAll_subgroup.setEnabled(True)
                self.ui.cancelAll_subgroup.setEnabled(True)
            else:
                self.ui.subgroupId.setEnabled(False)
                self.ui.selAll_subgroup.setEnabled(False)
                self.ui.cancelAll_subgroup.setEnabled(False)            
    
    def onGenerateClick(self):
        if isinstance(self.ui.chartArea.canvas, MplCanvas):
            self.clearElementoSelezionato()
        xMin = convert(self.ui.xMin.text(),"string")
        xMax = convert(self.ui.xMax.text(),"string")
        yMin = convert(self.ui.yMin.text(),"string")
        yMax = convert(self.ui.yMax.text(),"string")
        
        if (self.ui.tabs.currentIndex() == 1):
            self.myChart.build(self.ui.chartType.itemData(self.ui.chartType.currentIndex()).toInt()[0], self.ui.tableName.currentText(), self.ui.xField.currentText(), self.ui.yField.currentText(), xMin, xMax, yMin, yMax)
        else:
            currentChart = self.ui.chart.itemData(self.ui.chart.currentIndex()).toInt()[0]
            currentRaggrupId = None
            if (self.ui.raggrupId.isEnabled() or (not self.ui.raggrupId.isEnabled() and self.bCmdLine)):
                si = self.ui.raggrupId.selectedItems()
                if (si is not None and len(si)>0):
                    #self.myChart.selectedGroups = []
                    finalString = ""
                    for el in si:
                        #self.myChart.selectedGroups.append(el.text())
                        finalString += "'"+el.text()+"',"
                    finalString = finalString[:-1]
                    currentRaggrupId = convert(finalString, "string")
                else:
                    msg = ""
                    '''if (self.myChart.chartType == 5):
                        msg = "Si prega di selezionare un gruppo."
                    else:
                        msg = 'Si prega di selezionare almeno un gruppo.'
                    showMessage(self.myChart.currentEnvironment, 'critical','Errore', msg)'''
                    if (self.myChart.myCanvas is not None and self.myChart.myCanvas.ax is not None):
                        self.myChart.myCanvas.fig.delaxes(self.myChart.myCanvas.ax)
                        self.myChart.myCanvas.ax = None
                    self.myChart.redraw()
                    return
            
            if (currentChart > 0):
                self.myChart.buildByChart(currentChart, currentRaggrupId, xMin, xMax, yMin, yMax)
            self.myChart.myCanvas.show()
        
    def clearElementoSelezionato(self):
        self.ui.selectedGroup.setText('')
        self.ui.selectedSubgroup.setText('')
        self.ui.selectedX.setText('')
        self.ui.selectedY.setText('')
        if (self.myChart is not None):
            self.myChart.PointSelectorRemove()
        self.ui.showAnnotations.setCheckState(QtCore.Qt.Unchecked)
        self.ui.showAnnotations.setEnabled(False)
   
    def updateFieldValues(self, table):        
        self.ui.xField.clear()
        self.ui.yField.clear()
        fields = self.myPGDB.getFieldsByTable(table)
        if len(fields) > 0:
            for value in fields:
                self.ui.xField.addItem(value['column_name'], value['column_name'])
                self.ui.yField.addItem(value['column_name'], value['column_name'])
            self.ui.xField.setEnabled(True)
            self.ui.yField.setEnabled(True)
        else:
            self.ui.xField.setEnabled(False)
            self.ui.yField.setEnabled(False)
    
    def initTableSelect(self):
        tNames = self.myPGDB.getTables()
        if len(tNames) > 0:
            for v in tNames:
                self.ui.tableName.addItem(v['table_name'])
            self.ui.tableName.setEnabled(True)
            self.ui.tableName.activated[str].connect(self.updateFieldValues)
            self.updateFieldValues(self.ui.tableName.currentText())
        else:
            self.ui.tableName.setEnabled(False)
    
    def initChartGroupSelect(self):
        types = self.myPGDB.getChartTypes()
        if len(types) > 0:
            intero = None
            for v in types:
                intero = int(v['Id_tipo'])
                self.ui.chartType.addItem(v['tipo_grafico'], intero)
            self.ui.chartType.setEnabled(True)
        else:
            self.ui.chartType.setEnabled(False)        
    
    def setupCustomTab(self):
        self.initChartGroupSelect()
        self.initTableSelect()
        return
        
    def updateRaggrupId(self):
        self.ui.raggrupId.clear()
        currentData = self.ui.chart.itemData(self.ui.chart.currentIndex()).toInt()[0]
        valori = self.myPGDB.getAllRaggrupValuesUsingCustomQuery(currentData)
        if (valori is not None):
            if (len(valori) > 0):
                self.bRaggrup = True
                for v in valori:
                    self.ui.raggrupId.addItem(str(v))
                self.ui.raggrupId.setEnabled(True)
                self.ui.selAll.setEnabled(True)
                self.ui.cancelAll.setEnabled(True)
            else:
                self.bRaggrup = False
                self.ui.raggrupId.setEnabled(False)
                self.ui.selAll.setEnabled(False)
                self.ui.cancelAll.setEnabled(False)
        else:
            availableRaggrupId = self.myPGDB.getRaggrupIdsByChart(currentData)
            if (availableRaggrupId is None):
                self.bRaggrup = False
                return
            if (len(availableRaggrupId) > 0):
                self.bRaggrup = True
                valori = self.myPGDB.getAllRaggrupValues(currentData)
                for v in valori:
                    self.ui.raggrupId.addItem(str(v))
                self.ui.raggrupId.setEnabled(True)
                self.ui.selAll.setEnabled(True)
                self.ui.cancelAll.setEnabled(True)
            else:
                self.bRaggrup = False
                self.ui.raggrupId.setEnabled(False)
                self.ui.selAll.setEnabled(False)
                self.ui.cancelAll.setEnabled(False)
        #currentChart = self.ui.chart.itemData(self.ui.chart.currentIndex()).toInt()[0]
        chart = self.myPGDB.getChartById(currentData)        

        if (chart is None or len(chart)==0):
            return
        if (chart[0]['id_tipo'] == 5):
            self.myChart.toggleOptionsFields(False)
            return
        if (chart[0]['id_tipo'] == 2):
            self.myChart.toggleScale('x',False)
            return
        self.myChart.toggleOptionsFields(True)
    
    def updateDefaultCharts(self):
        self.ui.chart.clear()
        if (self.bArgs and self.args['c'] is not None):
            res = self.myPGDB.getChartByCode(self.args['c'])
            if (res is not None):
                id_graf = res[0]['id_graf']
                id_group = res[0]['id_group']
                index = self.ui.chartGroup.findData(id_group)
                if (index > -1):
                    self.ui.chartGroup.setCurrentIndex(index)
                    self.ui.chartGroup.setEnabled(False)
        currentData = self.ui.chartGroup.itemData(self.ui.chartGroup.currentIndex()).toInt()[0]
        availableCharts = self.myPGDB.getChartsByGroup(currentData)
        if len(availableCharts) > 0:
            for value in availableCharts:
                self.ui.chart.addItem(value['num_grafico']+" - "+value['nome_grafico'], int(value['id_graf']))
            self.ui.chart.setEnabled(True)            
        else:
            self.ui.chart.setEnabled(False)
        if (self.bArgs):
            index = self.ui.chart.findData(id_graf)
            if (index > -1):
                self.ui.chart.setCurrentIndex(index)
                self.ui.chart.setEnabled(False)
        self.onChartChange()
    
    def setupDefaultTab(self):
        groups = self.myPGDB.getChartGroups()
        if len(groups) > 0:
            self.ui.chartGroup.addItem('Clicca per selezionare', None)
            for v in groups:
                self.ui.chartGroup.addItem(v['gruppo'], int(v['id_group']))
            self.ui.chartGroup.setEnabled(True)
            #self.updateDefaultCharts()
        else:
            self.ui.chartGroup.setEnabled(False)
        self.ui.chartGroup.activated[str].connect(self.updateDefaultCharts)
        self.ui.chart.activated[str].connect(self.onChartChange)
        
    def resetInputArgs(self):
        self.ui.xgrid.setValue(5)
        self.ui.ygrid.setValue(5)
        
        self.ui.xMin.setText("")
        self.ui.yMin.setText("")
        self.ui.xMax.setText("")
        self.ui.yMax.setText("")
        
        self.ui.invert_xaxis.setCheckState(QtCore.Qt.Unchecked)
        self.ui.invert_yaxis.setCheckState(QtCore.Qt.Unchecked)
        
        self.ui.bGrid.setCheckState(QtCore.Qt.Checked)
        
        self.ui.pointsViewer.setCheckState(QtCore.Qt.Unchecked)
        
        self.ui.logxScale.setCheckState(QtCore.Qt.Unchecked)
        self.ui.logyScale.setCheckState(QtCore.Qt.Unchecked)
        
        self.myChart.xRotationTicks = 0
        self.myChart.lineWidth = 3
        
        self.args = {
            'c': None,#"7.1",
            'rid': None,#"I6OLXA0",
            'sid': None,#'2000-05-31',
            "tb": None,
            "minx": None,
            "maxx": None,
            "miny": None,
            "maxy": None,
            "xticks": None,
            "yticks": None,
            "x_rotation_ticks": None,
            "logx": False,
            "logy": False,
            "invx": False,
            "invy": False,
            "spt": False,
            "nogrid": False,
            "line_width": None,
            #"imp"
            #"imw"
            #"imh"
        }
        
    def setInputArgs(self, args):
        self.args = {
            'c': args.code_chart,#"7.1",
            'rid': args.raggrup_id,#"I6OLXA0",
            'sid': args.subgroup_id,#'2000-05-31',
            "tb": args.table,
            "minx": args.minimun_x,
            "maxx": args.maximun_x,
            "miny": args.minimun_y,
            "maxy": args.maximun_y,
            "xticks": convert(args.x_ticks, "int"),
            "yticks": convert(args.y_ticks, "int"),
            "x_rotation_ticks": convert(args.x_rotation_ticks, "int"),
            "logx": args.logaritmic_scale_x,
            "logy": args.logaritmic_scale_y,
            "invx": args.invert_xaxis,
            "invy": args.invert_yaxis,
            "spt": args.show_points,
            "nogrid": args.no_grid,
            "line_width": args.line_width,
            #"imp"
            #"imw"
            #"imh"
        }
        
        if (self.args['c'] is not None):
            self.bArgs = True
    
    def groupAndSubgroupInitializer(self, par, elem):
        if (self.args[par] is not None):
            items = []
            for gid in self.args[par]:
                more = gid.split(",")
                for m in more:
                    fi = elem.findItems(m, QtCore.Qt.MatchExactly)
                    if (len(fi)>0):
                        items.append(fi[0])
            for i in items:
                i.setSelected(True)
            
            elem.setEnabled(False)
            self.ui.selAll.setEnabled(False)
            self.ui.cancelAll.setEnabled(False)
            if (par == "sid"):
                self.ui.selAll_subgroup.setEnabled(False)
                self.ui.cancelAll_subgroup.setEnabled(False)
    
    def tickInitializer(self):
        if (self.args['xticks']  is not None):
            self.ui.xgrid.setValue(self.args['xticks'])
        if (self.args['x_rotation_ticks'] is not None):
            self.myChart.xRotationTicks = self.args['x_rotation_ticks']
        if (self.args['yticks']  is not None):
            self.ui.ygrid.setValue(self.args['yticks'])
    
    def inversionAxisInitializer(self):
        if (self.args['invx']):
            self.ui.invert_xaxis.setCheckState(QtCore.Qt.Checked)
        if (self.args['invy']):
            self.ui.invert_yaxis.setCheckState(QtCore.Qt.Checked)
    
    def gridInitializer(self):
        if (self.args['nogrid']):
            self.ui.bGrid.setCheckState(QtCore.Qt.Unchecked)
    
    def pointInitializer(self):
        if (self.args['spt']):
            self.ui.pointsViewer.setCheckState(QtCore.Qt.Checked)
    
    def logAxisInitializer(self):
        if (self.args['logx']):
            self.ui.logxScale.setCheckState(QtCore.Qt.Checked)
        if (self.args['logy']):
            self.ui.logyScale.setCheckState(QtCore.Qt.Checked)
    
    def limitInitializer(self):
        if (self.args['minx'] is not None):
            self.ui.xMin.setText(self.args['minx'])
        if (self.args['miny'] is not None):
            self.ui.yMin.setText(self.args['miny'])
        if (self.args['maxx'] is not None):
            self.ui.xMax.setText(self.args['maxx'])
        if (self.args['maxy'] is not None):
            self.ui.yMax.setText(self.args['maxy'])
    
    def lineWidthInitializer(self):
        if (self.args['line_width'] is not None):
            self.myChart.lineWidth = self.args['line_width']
    
    def __init__(self, args):
        QtGui.QMainWindow.__init__(self)
        self.ui = Ui_Arpapchart()
        self.ui.setupUi(self)
        self.myPGDB = db()
        self.setInputArgs(args)
        self.myChart = chart(self.ui.chartArea.canvas, self.ui)
        self.myChart.pointsViewer = self.ui.pointsViewer.isChecked()
        self.myChart.bGrid = self.ui.bGrid.isChecked()
        self.myChart.logxScale = self.ui.logxScale.isChecked()
        self.myChart.logyScale = self.ui.logyScale.isChecked()
        self.myChart.invert_xaxis = self.ui.invert_xaxis.isChecked()
        self.myChart.invert_yaxis = self.ui.invert_yaxis.isChecked()
        self.setupCustomTab()
        self.setupDefaultTab()
        self.ui.generateChart.clicked.connect( self.onGenerateClick ) 
        self.ui.groupFinder.textChanged.connect(lambda: self.findInQListWidget(self.ui.raggrupId, self.ui.groupFinder.text()))
        self.ui.subgroupFinder.textChanged.connect(lambda: self.findInQListWidget(self.ui.subgroupId, self.ui.subgroupFinder.text()))
        self.ui.xMin.textChanged.connect(lambda: self.onLimitChange('x'))
        self.ui.xMax.textChanged.connect(lambda: self.onLimitChange('x'))
        self.ui.yMin.textChanged.connect(lambda: self.onLimitChange('y'))
        self.ui.yMax.textChanged.connect(lambda: self.onLimitChange('y'))
        self.ui.xgrid.valueChanged.connect(lambda: self.myChart.setLocator('x',self.ui.xgrid.text(), True))
        self.ui.ygrid.valueChanged.connect(lambda: self.myChart.setLocator('y',self.ui.ygrid.text(), True))
        self.ui.pointsViewer.stateChanged.connect(lambda: self.myChart.togglePoints(self.ui.pointsViewer.isChecked()) )
        self.ui.bGrid.stateChanged.connect(lambda: self.myChart.toggleGrid(self.ui.bGrid.isChecked()) )
        self.ui.invert_xaxis.stateChanged.connect(lambda: self.myChart.toggleInversionAxis('x',self.ui.invert_xaxis.isChecked(), True) )
        self.ui.invert_yaxis.stateChanged.connect(lambda: self.myChart.toggleInversionAxis('y',self.ui.invert_yaxis.isChecked(), True) )
        self.ui.logxScale.stateChanged.connect(lambda: self.onScaleCheck('x') )
        self.ui.logyScale.stateChanged.connect(lambda: self.onScaleCheck('y') )
        self.ui.showAnnotations.stateChanged.connect(lambda: self.myChart.toggleAnnotations(self.ui.showAnnotations.isChecked()))
        self.ui.selAll.clicked.connect(lambda: self.myChart.toggleAllGroups(True) )
        self.ui.cancelAll.clicked.connect(lambda: self.myChart.toggleAllGroups(False) )
        #self.ui.raggrupId.setSelectionMode(QtGui.QAbstractItemView.MultiSelection)
        self.ui.raggrupId.selectionModel().selectionChanged.connect(self.listSubgroups)
        self.ui.selAll_subgroup.clicked.connect(lambda: self.myChart.toggleAllSubgroups(True) )
        self.ui.cancelAll_subgroup.clicked.connect(lambda: self.myChart.toggleAllSubgroups(False) )
        self.ui.subgroupId.selectionModel().selectionChanged.connect(self.onSelectedSubgroup)
        self.ui.subgroupId.setSelectionMode(QtGui.QAbstractItemView.MultiSelection)
        if (self.bArgs):
            self.bCmdLine = True
            self.updateDefaultCharts()
            self.groupAndSubgroupInitializer('rid',self.ui.raggrupId)
            self.groupAndSubgroupInitializer('sid',self.ui.subgroupId)
            self.lineWidthInitializer()
            self.limitInitializer()
            self.logAxisInitializer()
            self.pointInitializer()
            self.inversionAxisInitializer()
            self.gridInitializer()
            self.tickInitializer()
        self.bArgs = False
        
class launcher:
    def __init__(self, args):
        app = QtGui.QApplication(sys.argv)        
        e = APP(args)
        e.show()
        sys.exit(app.exec_())

#launcher()