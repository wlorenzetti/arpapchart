#!/usr/bin/env python
# -*- coding: UTF-8 -*-
'''
Arpapchart - Small application for build different type of chart. Based on Matplotlib library you can build different type chart using database PostgreSql.
It can be used in 3 way:
1) via QT Graphics interface
2) via Command Line
2) via Browser


@author:     Walter Lorenzetti, Francesco Belllina
@copyright:  2013-2014 GIS3W. All rights reserved.
@license:    GPL
@contact:    lorenzetti@gis3w.it
'''

import sys
import os

from arpapchart.globals import *
from arpapchart.utils import *

from argparse import ArgumentParser, ArgumentTypeError
from argparse import RawDescriptionHelpFormatter
from arpapchart.runserver.runserver import webRun
#from arpapchart.cli import cli

__all__ = []
__version__ = VERSION
__date__ = '2013-11-19'
__updated__ = '2014-047-04'



def main(argv=None): # IGNORE:C0111
    '''Command line options.'''
    
    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)
    program_shortdesc = 'Arpapchart - Small application for build different type of chart. Based on Matplotlib library you can build different type chart using database PostgreSql.'
    program_license = '''%s

  Created by Walter Lorenzetti and Francesco bellina on %s.
  Copyright 2013-2014 GIS3W. All rights reserved.
  
  Licensed under the GPL
  http://www.apache.org/licenses/LICENSE-2.0
  
  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.

USAGE
''' % (program_shortdesc, str(__date__))

    try:
        # Setup argument parser
        parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
        group_cli = parser.add_argument_group('CLI', 'Command and options for COMMAND LINE INTERFACE')
        
        group_cli.add_argument("-c", "--code-chart", action='store',help="set code group ",metavar='CODE CHART')
        group_cli.add_argument("-rid", "--raggrup-id",help="set for reggrup id values",nargs='*',metavar='RAGGRUP ID VALUES')
        group_cli.add_argument("-sid", "--subgroup-id",help="set for subgroup id values",nargs='*',metavar='SUBGROUP ID VALUES')
        group_cli.add_argument("-tb", "--table", action='store',help="set table for chart",nargs='?',metavar='TABLE NAME')
        group_cli.add_argument("-minx", "--minimun-x", action='store',help="Set the minimum x value of the chart",metavar='MIN X VALUE')
        group_cli.add_argument("-maxx", "--maximun-x", action='store',help="Set the maximum x value of the chart",metavar='MAX X VALUE')
        group_cli.add_argument("-miny", "--minimun-y", action='store',help="Set the minimum y value of the chart",metavar='MIN Y VALUE')
        group_cli.add_argument("-maxy", "--maximun-y", action='store',help="Set the maximum y value of the chart",metavar='MAX Y VALUE')
        group_cli.add_argument("-xticks", "--x-ticks", action='store',help="Set the quantity of ticks on X axis",metavar='X Ticks')
        group_cli.add_argument("-yticks", "--y-ticks", action='store',help="Set the quantity of ticks on Y axis",metavar='Y Ticks')
        group_cli.add_argument("-x_rotation_ticks", "--x_rotation_ticks", action='store',help="Set rotation for X Ticks",metavar='X Ticks')
        group_cli.add_argument("-logx", "--logaritmic-scale-x", action='store_true',help="Scale logarithmic for x axis")
        group_cli.add_argument("-logy", "--logaritmic-scale-y", action='store_true',help="Scale logarithmic for y axis")        
        group_cli.add_argument("-invx", "--invert-xaxis", action='store_true',help="Invert X Axis")
        group_cli.add_argument("-invy", "--invert-yaxis", action='store_true',help="Invert Y Axis")
        group_cli.add_argument("-nogrid", "--no-grid", action='store_true',help="Show grid on chart")
        group_cli.add_argument("-spt", "--show-points", action='store_true',help="Show line points on chart")
        group_cli.add_argument("-line_width", "--line-width", action='store_true',help="Set line's width")
        group_cli.add_argument("-imp", "--image-path", action='store',help="Set path for image to save")
        group_cli.add_argument("-imw", "--image-width", action='store',help="Set image with in pixel")
        group_cli.add_argument("-imh", "--image-height", action='store',help="Set image height in pixel")
        
        group_desktop = parser.add_argument_group('DESKTOP', 'Run application without arguments for run DESKTOP APPLICATION')        
        
        group_web = parser.add_argument_group('WEB', 'Command and options for WEB APPLICATION')
        group_web.add_argument("-rsrv", "--run-server", action=webRun,help="run a webserver",nargs='?',metavar='PORT',default=5000,type=int)

        # Process arguments
        args = parser.parse_args()    
        #without arguments start desktop application
        argv.pop(0)  
        if not len(argv) or '-rsrv' not in argv:
            from arpapchart.desktop.desktop import launcher
            launcher(args)


    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0
    except Exception, e:
        indent = len(program_name) * " "
        sys.stderr.write(program_name + ": " + str(e) + "\n")
        sys.stderr.write(indent + "  for help use --help")
        return 2
    
if __name__ == "__main__":
    sys.exit(main())