#!/usr/bin/env python

from setuptools import setup, find_packages

setup(name='Arpapchart',
      version='0.1',
      description='Arpapchart - Small application for build different type of chart. Based on Matplotlib library you can build different type chart using database PostgreSql. It can be used in 3 way: 1) via QT Graphics interface 2) via Command Line 2) via Browser',
      author='Walter Lorenzetti, Francesco bellina',
      author_email='lorenzetti@gis3w.it',
      url='http://www.gis3w.it',
      packages = find_packages(),
      package_data = {
            '': ['*.*']
        },
      scripts = ['apchart.py'],
      install_requires = ['matplotlib','psycopg2','flask','numpy'],
      entry_points = {
        'console_scripts': [
            'apchart = apchart:main',
        ]
    }
     )
